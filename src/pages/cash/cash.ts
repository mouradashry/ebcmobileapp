import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

import { SuccessMessage } from '../success-message/success-message';
import { FailureMessage } from '../failure-message/failure-message';

import { HomePage } from '../home/home';

import { GlobalVariablesService } from '../../app/services/global-variables.service';
import { ApiService } from '../../app/services/api.service';
import { TranslationService } from '../../app/services/translation.service';


@IonicPage()
@Component({
  selector: 'page-cash',
  templateUrl: 'cash.html',
})
export class Cash {

  mPin: string;
  cash: any;
  approveSubUrl: string = '/transaction/completeTransaction';
  rejectSubUrl: string = '/transaction/rejectTransaction';
  disableRejectBtn: boolean;
  disableAcceptBtn: boolean;
  description: string;

  constructor(public navCtrl: NavController,
    public params: NavParams, public alertCtrl: AlertController,
    private globals: GlobalVariablesService, private api: ApiService,
    private translation: TranslationService) {
  }

  async ionViewCanEnter() {
    this.cash = this.params.get('cash');
    if (this.cash) {
      await this.addThisCashToNotifications(this.cash);
      return true;
    }
    return false;
  }

  async  ionViewDidLoad() {
    let translated = await this.translation.getValue(["title.cash.1", "title.cash.2"])
    this.description = this.cash.message ||
      `${this.cash.title} ${translated["title.cash.1"]} ${this.cash.amount} ${translated["title.cash.2"]} ${this.cash.merchantWalletId}`
  }

  /**
   * if this page was opened directly when the user was opening the app
   * this function will send to the notification object that this is an opened notification
   * so , that the view button should appear for it
   * @param trxData 
   */
  async addThisCashToNotifications(trxData: any) {
    let last4Requests = await this.globals.get(this.globals.LAST4Requests);

    // handling deeplinking if the application opened directly here 
    // then initialize the variable of the notifications
    if (!last4Requests)
      last4Requests = {};

    // if does not exist add it 
    if (!last4Requests[this.cash.refNum]) {
      last4Requests[this.cash.refNum] = {
        viewed: true
      };

      // then update the list 
      await this.globals.update(this.globals.LAST4Requests, last4Requests);
    }

    // if exist which should always or in most cases happen then just change it's state
    // to viewed true
    if (last4Requests[this.cash.refNum]) {
      last4Requests[this.cash.refNum].viewed = true;
      await this.globals.update(this.globals.LAST4Requests, last4Requests);
    }

    return;
  }

  cancelTransaction() {
    this.disableRejectBtn = true;
    this.disableAcceptBtn = true;
    this.globals.presentLoading();

    let trxRefNum = this.cash.refNum;
    delete this.cash;
    this.api.postData(this.rejectSubUrl, { trxRefNum })
      .subscribe(async (res) => {
        this.globals.presentLoading();
        if (res.status == 200)
          this.globals.presentToast(res.data.message);

        // printing error in case of not 200 status
        if (res.status !== 200)
          this.globals.presentToast(res.data.error);

        await this.api.updateLast4requests(true);
        this.globals.dismissLoading();
        this.navCtrl.setRoot(HomePage);
      });

  }

  validate() {

    if (this.mPin) {
      if (this.mPin.length != 6) {
        this.globals.presentToast('toast.2');
      }
      else {
        this.performTransaction(this.mPin)
      }

    }
    else if (!this.mPin) {
      this.globals.presentToast('toast.3');
    }
  }

  performTransaction(mPin) {
    this.disableAcceptBtn = true;
    this.globals.presentLoading();
    this.api.postData(this.approveSubUrl, {
      mpin: mPin, requestRefNum: this.cash.refNum,
      type: this.cash.type
    }).subscribe(async (res) => {

      this.globals.presentLoading();

      if (res.status === 200) {
        await this.globals.update('balance', res.data.balanceAfterFees);
        this.navCtrl.setRoot(SuccessMessage, { message: res.data.message });
      }

      if (res.status === 400 && (res.data.endTrx === true || res.data.endTrx === undefined)) {
        this.navCtrl.setRoot(FailureMessage, { message: res.data.error });
      }

      if (res.status === 400 && res.data.endTrx === false) {
        this.globals.dismissLoading();
        this.disableAcceptBtn = false;
        return await this.globals.presentToast(res.data.error);
      }

      await this.api.updateLast4requests(true);
      this.globals.dismissLoading();
      this.disableAcceptBtn = false;
    });
  }


}
