import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cash } from './cash';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    Cash,
  ],
  imports: [
    IonicPageModule.forChild(Cash),
    TranslateModule.forChild()
  ],
  exports: [
    Cash
  ]
})
export class CashModule {}
