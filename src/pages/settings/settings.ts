import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController } from 'ionic-angular';
import { LogOutPage } from '../logout/logout';
import { ChangeMpinPage } from '../change-mpin/change-mpin';
import { TranslationService } from '../../app/services/translation.service';
import { GlobalVariablesService } from '../../app/services/global-variables.service';
import { ApiService } from '../../app/services/api.service'
@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class Settings {
  version = this.globals.version;
  constructor(public navCtrl: NavController,
    public alertController: AlertController,
    public translate: TranslationService,
    public globals: GlobalVariablesService,
    public apiservice: ApiService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Settings');
  }

  changeMpin() {
    this.navCtrl.push(ChangeMpinPage)
  }

  back() {
    this.navCtrl.pop();
  }

  getWantedLang(language: string) {

    if (language == 'en')
      return 'ar';

    return 'en';
  }

  async changelng() {
    let lng = await this.translate.getValue('lng');
    let wantedlanguage = this.getWantedLang(lng);
    let response = await this.apiservice.postData('/changeLanguage', { lang: wantedlanguage }).toPromise();

    if (response.status != 200)
      return this.globals.presentToast(response.message)

    if (response.status === 200 && lng === 'en')
      return this.translate.toAr();

    if (response.status == 200 && lng === 'ar')
      return this.translate.toEng();

  }

  async logout() {
    let translated = await this.translate.getValue([
      'Logout', 'alert.setting.1', 'Yes', 'No', 'Cancel'
    ]);

    await this.globals.update(this.globals.LAST4Requests, {});

    this.alertController.create({
      title: translated['Logout'],
      message: translated['alert.setting.1'],
      buttons: [{
        text: translated['Yes'],
        handler: data => {
          this.navCtrl.push(LogOutPage);
        }
      }, {
        text: translated['No'],
        role: translated['Cancel']
      }]
    }).present();
  }

}
