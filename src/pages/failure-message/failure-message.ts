import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-failure-message',
  templateUrl: 'failure-message.html',
})
export class FailureMessage {

  message: string = "";
  destination: string;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.message = this.navParams.get("message");
  }

  done() {
    this.navCtrl.setRoot(HomePage);
  }

}
