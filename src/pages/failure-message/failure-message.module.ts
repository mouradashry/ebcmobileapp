import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FailureMessage } from './failure-message';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    FailureMessage,
  ],
  imports: [
    IonicPageModule.forChild(FailureMessage),
    TranslateModule.forChild()
  ],
  exports: [
    FailureMessage
  ]
})
export class FailureMessageModule {}
