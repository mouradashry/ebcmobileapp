import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GlobalVariablesService } from '../../app/services/global-variables.service'
import { NFC, Ndef } from '@ionic-native/nfc';
import { Purchase } from '../purchase/purchase'

@Component({
  selector: 'page-nfc',
  templateUrl: 'nfc.html',
})
export class Nfc {
  amount: string;
  convenience: string;
  tip: boolean;
  reference1: string;
  reference2: string;
  tipOrConvenience: 'tip' | 'convenience' | 'none' = 'none';
  generateQrcodeUrl: string = '/generateQrcode';
  userType: string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private cdRef: ChangeDetectorRef,
    public globals: GlobalVariablesService,
    private nfc: NFC, private ndef: Ndef) {
  }

  ionViewWillEnter() {
    this.getUserType();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GenerateQrcode');
  }

  back() {
    this.navCtrl.pop();
  }

  async shareNfc() {
    let nfcAvailable = await this.nfcIsAvailable();
    if (!nfcAvailable)
      return this.globals.presentToast('NFC is not available');

    let amount;
    if (this.amount)
      amount = parseFloat(this.amount);

    if (!this.isValid())
      return;

    let walletId = await this.globals.get('walletId')

    let data = {
      walletId:walletId,
      amount: amount,
      reference1: this.reference1,
      reference2: this.reference2,
      tip: this.tipOrConvenience === 'tip' ? true : null,
      convenience: this.tipOrConvenience === 'convenience' ? this.convenience : null
    }

    let message = this.ndef.textRecord(JSON.stringify(data), 'en-US', null);
    try {
      this.globals.presentToast('sharing NFC');
      let res = await this.nfc.share([message]);
      console.log('success', res);
    }
    catch (err) {
      console.log('error', err);
    }


  }

  isValid() {
    if (this.tipOrConvenience === 'convenience') {
      if (!this.convenience || this.convenience === '') {
        this.globals.presentToast('Convenience field is required')
        return false;
      }
    }
    return true;
  }

  async getUserType() {
    this.userType = await this.globals.get('user_type');
  }

  async nfcIsAvailable() {
    try {
      let available = await this.nfc.enabled();
      console.log('NFC is available', available);
      return true;
    }
    catch (error) {
      console.log('error', error);
      return false;
    }
  }

  async unshareNfc(){
    let nfcAvailable = await this.nfcIsAvailable();
    if (nfcAvailable)
      return await this.nfc.unshare();
  }

  detectAmountChange(value) {
    this.cdRef.detectChanges();
    this.amount = this.globals.detectChange(value, this.amount, 6);
  }

  detectConvenienceChange(value) {
    this.cdRef.detectChanges();
    this.convenience = this.globals.detectChange(value, this.convenience, 6);
  }

  ngOnDestroy() {
    this.unshareNfc();
  }

}
