import { Component, OnInit, ViewChild } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { ApiService } from '../../../app/services/api.service';
import { GlobalVariablesService } from '../../../app/services/global-variables.service';
import * as qrcode from 'qrcode';
import { DynamicQrCode } from '../dynamic-qrcode/dynamic-qrcode';


@IonicPage()
@Component({
    selector: 'page-static-qrcode',
    templateUrl: 'static-qrcode.html',
})
export class StaticQrCode implements OnInit {

    generateQrcodeUrl: string = '/generateQrcode';
    @ViewChild('canv') canv;

    constructor(public navCtrl: NavController, public api: ApiService, public globals: GlobalVariablesService) {
    }

    back() {
        this.navCtrl.pop();
    }

    async ngOnInit() {
        await this.globals.get('fullname');
        await this.globals.get('walletId');

        this.globals.presentLoading();
        let body = {
            amount: null,
            qrcodeType: 'static'
        };

        let res = await this.api.postData(this.generateQrcodeUrl, body).toPromise()
        let qrcode = res.data.qrcode;

        await this.generateQrImg(qrcode);
    }

    generateQrImg(payload) {
        return new Promise(resolve => {
            qrcode.toCanvas(this.canv.nativeElement, payload,
                (err: any) => resolve(err));
        });
    }

    goToGenerateDynamicCode() {
        this.navCtrl.push(DynamicQrCode);
    }
}