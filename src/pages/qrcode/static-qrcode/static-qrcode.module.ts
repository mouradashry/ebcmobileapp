import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StaticQrCode } from './static-qrcode';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    declarations: [
        StaticQrCode,
    ],
    imports: [
        IonicPageModule.forChild(StaticQrCode),
        TranslateModule.forChild()
    ],
    exports: [
        StaticQrCode
    ]
})
export class StaticQrCodeModule { }
