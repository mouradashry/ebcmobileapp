import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalVariablesService } from '../../../app/services/global-variables.service';
import { ApiService } from '../../../app/services/api.service';
import { QrDetailsPage } from '../../qr-details/qr-details';


@IonicPage()
@Component({
  selector: 'page-dynamic-qrcode',
  templateUrl: 'dynamic-qrcode.html',
})
export class DynamicQrCode {
  amount: string;
  convenience: string;
  reference1: string;
  reference2: string;
  tipOrConvenience: 'tip' | 'convenience' | 'none' = 'none';
  generateQrcodeUrl: string = '/generateQrcode';


  openedLabelCounter = 0;

  /**
   * labels booleans
   */
  tips = false;
  billNumber = false;
  mobileNumber = false;
  storeLabel = false;
  loyaltyNumber = false;
  referenceLabel = false;
  customerLabel = false;
  terminalLabel = false;
  purposeOfTransaction = false;

  /**
   * labels values
   */
  labels = {
    billNumberField: null,
    mobileNumberField: null,
    storeLabelField: null,
    loyaltyNumberField: null,
    referenceLabelField: null,
    customerLabelField: null,
    terminalLabelField: null,
    purposeOfTransactionField: null,
  };

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private cdRef: ChangeDetectorRef,
    public globals: GlobalVariablesService, private api: ApiService) {
  }

  back() {
    this.navCtrl.pop();
  }

  async generateCode() {
    let amount;
    if (this.amount)
      amount = parseFloat(this.amount);

    if (!this.isValid())
      return;

    this.globals.presentLoading();

    let body = {
      amount: amount,
      tip: this.tips === true ? true : null,
      qrcodeType: 'dynamic'
    }

    // merging the above body and the label values
    Object.assign(body, this.getLabelValues());

    this.api.postData(this.generateQrcodeUrl, body).subscribe(async (res) => {
      if (res.status == 200)
        return this.navCtrl.push(QrDetailsPage, { body: body, qrCode: res.data.qrcode });

      this.globals.presentToast(res.data.error)
    });

  }

  isValid() {
    if (this.tipOrConvenience === 'convenience') {
      if (!this.convenience || this.convenience === '') {
        this.globals.presentToast('toast.8')
        return false;
      }
    }
    return true;
  }

  detectAmountChange(value) {
    this.cdRef.detectChanges();
    this.amount = this.globals.detectChange(value, this.amount, 6);
  }

  detectConvenienceChange(value) {
    this.cdRef.detectChanges();
    this.convenience = this.globals.detectChange(value, this.convenience, 6);
  }

  /**
   * changing a variable which count how many labels are opened in the current moment
   */
  toggleChanged(checkerValue: boolean) {
    if (checkerValue) {
      return this.openedLabelCounter++;
    }

    this.openedLabelCounter--;
  }

  /**
   * disable the close labels if the number of opened lables reached 6
   * @param labelBoolean 
   */
  isDisabled(labelBoolean: string) {
    if (!this.labels[labelBoolean] && this.openedLabelCounter >= 6)
      return true;

    return false;
  }

  /**
   * collect label values and fields
   */
  getLabelValues() {
    let keys = Object.keys(this.labels);
    let labelObject = {};

    for (let key of keys) {
      // if label was not opened then just go to the next label
      if (!this.labels[key])
        continue;

      // // testing of lable field is empty or with empty value to put astericks
      if (this[`${key}Field`] == null || (this[`${key}Field`].toString()).trim() == '') {
        labelObject[key] = '***';
        continue;
      }

      // if label had a value put it
      labelObject[key] = this[`${key}Field`];
    }

    return labelObject;
  }

}
