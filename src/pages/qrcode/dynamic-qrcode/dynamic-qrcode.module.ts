import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DynamicQrCode } from './dynamic-qrcode';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    DynamicQrCode,
  ],
  imports: [
    IonicPageModule.forChild(DynamicQrCode),
    TranslateModule.forChild()
  ],
  exports: [
    DynamicQrCode
  ]
})
export class DynamicQrCodeModule {}
