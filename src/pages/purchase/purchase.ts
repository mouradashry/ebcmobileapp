import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { FailureMessage } from '../failure-message/failure-message';
import { TransferConfirmation } from '../transfer-confirmation/transfer-confirmation';
import { ApiService } from '../../app/services/api.service';
import { GlobalVariablesService } from '../../app/services/global-variables.service';
import { FavouriteService } from '../../app/services/favourite.service';
import { HomePage } from '../home/home';
import { ScanQrcodeService } from '../../app/services/scanQrcode.service';
import { ContactsService } from '../../app/services/contacts.service';
import { TrxStep1Service } from '../../app/services/trx-step1.service';
import { Validator } from '../../app/services/validator.service';

@Component({
  selector: 'page-purchase',
  templateUrl: 'purchase.html',
})
export class Purchase {
  amount: string;
  dataSub: any;
  walletId: string;
  title: string = 'purchase';
  description: string;
  disableWalletId: boolean;
  disableAmount: boolean;
  disableRef1: boolean = false;
  disableRef2: boolean = false;
  merchantName: string = null;

  /**
   * flag to determine if you are coming from a normal click or from a qrcode scan defaults to false
   */
  isComingFromQrCode = false;

  /**
   * tips flag
   */
  tips = false;

  /**
   * tips value if exist 
   */
  tipsField: number = null;
  convenience: any
  /**
   * additional data field values
   */
  billNumberField = null;
  mobileNumberField = null;
  storeLabelField = null;
  loyaltyNumberField = null;
  referenceLabelField = null;
  customerLabelField = null;
  terminalLabelField = null;
  potField = null;

  additionalData: any = {
    reference1: null,
    reference2: null,
    tips: null,
    convenience: null
  };

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public alertCtrl: AlertController, public api: ApiService,
    public globals: GlobalVariablesService, private cdRef: ChangeDetectorRef,
    private favouriteService: FavouriteService, private scanqr: ScanQrcodeService,
    private contactsService: ContactsService, private trxStep1Service: TrxStep1Service,
    public validator: Validator) {
    this.getIncomingData();
    //this.description = 'Pay any amount to a merchant from your wallet';
  }

  async validate() {

    if (this.walletId) {
      if (!(this.walletId.length == 11 || this.walletId.length == 9))
        return this.globals.presentToast('toast.14');
    }
    else
      return this.globals.presentToast('toast.15');

    if (this.isComingFromQrCode && this.tips && !this.globals.isValidTip(this.tipsField))
      return;

    if (!this.isComingFromQrCode && this.additionalData.tips && !this.globals.isValidTip(this.additionalData.tips))
      return;

    if (!this.amount)
      return this.globals.presentToast("toast.7");

    // validate that all fields are filled by the user
    // if ((this.isComingFromQrCode && await this.validateEmptyFields()) || !this.isComingFromQrCode)
    return await this.transfer();
  }

  async transfer() {
    let data = {
      amount: this.amount,
      recieverMobile: this.walletId,
      additionalData: this.additionalData
    }

    // if qrcode then make a new body according 
    if (this.isComingFromQrCode)
      data = this.getBody(this.additionalData, data);

    let res = await this.trxStep1Service.trxStep1(data);
    if (res[0])
      return await this.navCtrl.push(TransferConfirmation, res[1]);
    await this.navCtrl.push(FailureMessage, res[1]);
  }

  /**
   * if the request was coming from a qrcode scan then this function will make the 
   * body of the request
   * @param additionalData 
   * @param data 
   */
  getBody(additionalData: any, data: any) {

    if (this.tips) {
      data['tips'] = this.tipsField;
      data['additionalData']['tips'] = this.tipsField;
    }

    if (this.convenience) {
      data['convenience'] = this.convenience
      data['additionalData']['convenience'] = this.convenience;
      this.additionalData['convenience'] = this.convenience;
    }
    data['additionalData'] = this.getAdditionalData(additionalData);
    data['additionalData']['qrcode'] = true;
    return data;
  }

  /**
   * getting data values from fields 
   * @param additionalData 
   */
  getAdditionalData(additionalData: any) {
    let keys = Object.keys(additionalData);
    let data = {};
    for (let key of keys) {
      if (this[`${key}Field`])
        data[key] = this[`${key}Field`];
    }
    return data;
  }

  getIncomingData() {
    let qrcodeData = this.navParams.get('qrcode');
    let nfcData = this.navParams.get('nfc');
    let favouriteData = this.navParams.get('favourite');

    if (qrcodeData || nfcData)
      return this.setQrcodeNfcData(qrcodeData || nfcData);

    if (favouriteData)
      return this.setFavData(favouriteData);
  }

  setQrcodeNfcData(data) {
    this.isComingFromQrCode = true;
    this.amount = data.amount;
    this.walletId = data.walletId;
    this.tips = data.tip;
    this.convenience = data.convenience
    this.merchantName = data.merchantName;

    this.clearInputFields();

    // make qrcode additional data
    this.additionalData = {
      billNumber: data.additionalData.billNumber,
      mobileNumber: data.additionalData.mobileNumber,
      storeLabel: data.additionalData.storeLabel,
      loyaltyNumber: data.additionalData.loyaltyNumber,
      referenceLabel: data.additionalData.referenceLabel,
      customerLabel: data.additionalData.customerLabel,
      terminalLabel: data.additionalData.terminalLabel,
      pot: data.additionalData.pot
    };

    // calling a function to set the additional data fields and initialize their values
    this.setAdditionalDataFields(this.additionalData);

    if (this.amount) this.disableAmount = true;
    if (this.walletId) this.disableWalletId = true;
    if (this.additionalData.reference1) this.disableRef1 = true;
    if (this.additionalData.reference2) this.disableRef2 = true;
  }

  /**
   * @description function to set the additional data fields 
   * @param additionalData 
   */
  setAdditionalDataFields(additionalData: any) {
    let keys = Object.keys(additionalData);
    for (let key of keys) {

      if (!additionalData[key] || additionalData[key] === '***')
        continue;

      this[`${key}Field`] = additionalData[key];
    }

  }

  clearInputFields() {
    this.billNumberField = null;
    this.mobileNumberField = null;
    this.storeLabelField = null;
    this.loyaltyNumberField = null;
    this.referenceLabelField = null;
    this.customerLabelField = null;
    this.terminalLabelField = null;
    this.potField = null;
    this.tipsField = null;
  }

  setFavData(favouriteData) {
    this.amount = favouriteData.amount;
    this.walletId = favouriteData.receiver;
    let extraData = favouriteData.data;
    if (extraData) this.additionalData = extraData;
  }

  async scanQrcode() {
    let data = await this.scanqr.scan();
    if (data)
      this.setQrcodeNfcData(data);

  }

  async favourite() {
    let transType = await this.trxStep1Service.getTrxType();
    let data = {
      title: this.title,
      trx_type: transType,
      receiver: this.walletId,
      amount: this.amount,
      service_type: "transaction",
      data: this.additionalData
    }
    this.favouriteService.addandNameFavourite(data);
  }

  /**
   * make fields disabled or not
   * @param checkValue 
   */
  isDisabled(checkValue: any) {
    if (!this.isComingFromQrCode)
      return false;

    return !(checkValue === '***');
  }

  /**
   * validate that all fields that are required are not empty
   */
  async validateEmptyFields() {
    let additionalData = this.additionalData;
    if (this.tips)
      additionalData = Object.assign({ 'tips': this.tipsField || '***' }, additionalData);
    let keys = Object.keys(additionalData);
    for (let key of keys) {
      // get the key word in which i will send to the toast 
      // get its display name 
      // put it to the translate 
      if (this.validator.isNull(this[`${key}Field`]) && additionalData[key] === '***') {
        this.globals.presentToast(`${await this.globals.getAdditionalDataDisplayName(key)} ${await this.globals.translateWord('required')}`)
        return false;
      }
    }

    return true;
  }

  /**
  * function to get user contacts
  */
  getContact() {
    this.contactsService.getContact(this, 'walletId');
  }

  /**
   * navigation functions
   */
  back() {
    this.navCtrl.pop();
  }

  home() {
    this.navCtrl.push(HomePage);
  }

  /**
   * validator functions
   */
  detectNumberChange(value) {
    this.cdRef.detectChanges();
    this.walletId = this.globals.detectChange(value, this.walletId, 11);
  }

  detectAmountChange(value) {
    this.cdRef.detectChanges();
    this.amount = this.globals.detectChange(value, this.amount, 6);
  }

  detectTipsChange(value) {
    this.cdRef.detectChanges();
    this.additionalData.tips = this.globals.detectChange(value, this.additionalData.tips, 6);
  }

}