import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SignupIntermediate } from '../signup-intermediate/signup-intermediate'
import { LoginPage } from '../login/login';
import { GlobalVariablesService } from '../../app/services/global-variables.service';

@Component({
  selector: 'page-splash',
  templateUrl: 'splash.html',
})
export class Splash {
  
  theme = this.globals.theme;
  src: string = `assets/${this.theme}/logo.png`

  constructor(public splashScreen: SplashScreen,public navCtrl: NavController , private globals : GlobalVariablesService){}


  ionViewDidEnter() {
    //hide native splash screen
    this.splashScreen.hide();
  }

  ngOnInit() {
    setTimeout(() => {
      this.navCtrl.push(LoginPage);
      },6000);
  }
  
  // SignIn(){
  //   this.navCtrl.push(LoginPage);
  // }

  // signUp(){
  //   this.navCtrl.push(SignupIntermediate);
  // }

}