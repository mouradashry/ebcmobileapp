import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OnlineCardsPage } from './online-cards';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    OnlineCardsPage,
  ],
  imports: [
    IonicPageModule.forChild(OnlineCardsPage),
    TranslateModule.forChild()
  ],
  exports: [
    OnlineCardsPage
  ]
})
export class OnlineCardsPageModule {}
