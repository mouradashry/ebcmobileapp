import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalVariablesService } from '../../app/services/global-variables.service'
import { ApiService } from '../../app/services/api.service'
import { VccEnquire } from '../vcc-enquire/vcc-enquire';

/**
 * Generated class for the OnlineCardsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
	selector: 'page-online-cards',
	templateUrl: 'online-cards.html',
})
export class OnlineCardsPage {
	step: number = 1;
	amount;
	card;
	mpin;
	fees;
	comm;
	totalBalance;
	totalAmount;
	refNum;
	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		private cdRef: ChangeDetectorRef,
		public globals: GlobalVariablesService,
		private api: ApiService) {

	}

	Mpin() {
		this.navCtrl.push(VccEnquire);
	}

	back() {
		this.navCtrl.pop();
	}

	async getStepOneData() {
		let res = await this.api.postData('/transfer/vcc/stepOne', {amount: this.amount}).toPromise();

		if (res.status === 200) {
			this.fees = res.data.result.senderOk.totalFees;
			this.refNum = res.data.result.requestRefNum;
			this.comm = res.data.result.senderOk.totalComms;
			this.totalAmount = res.data.result.senderOk.totalAmount;
			this.totalBalance = res.data.result.senderOk.newBalance;
			this.step = 2;
			return;
		}

		this.toStep1();
		this.globals.presentToast(res.data.error);
	}

	async generateCard() {
		let body = {
			requestRefNum: this.refNum,
			amount: this.amount,
			mpin: this.mpin
		};

		let res = await this.api.postData('/transfer/vcc/generateCard', body).toPromise();

		if (res.status === 200) {
			this.card = {
				number: this.formatCardNumber(res.data.card.PAN),
				cvv: res.data.card.CVC,
				exp: res.data.card.expiryDate
			}
			this.step = 3;
			return;
		}

		this.globals.presentToast(res.data.message || res.data.error);
	}

	toStep1() {
		this.step = 1
		this.mpin = null;
	}

	async toStep2() {
		await this.globals.presentLoading();
		await this.getStepOneData();
	}

	async toStep3() {

		if (!this.mpin) {
			this.globals.presentToast('toast.3', 3);
			return false;
		}
		if (this.mpin.length !== 6) {
			this.globals.presentToast('toast.2', 3);
			return false;
		}

		this.globals.presentLoading();
		await this.generateCard();

	}

	formatCardNumber(card_number) {
		card_number = String(card_number);
		let formatted_card_number = '';
		for (let i = 0; i < 4; i++) {
			formatted_card_number += card_number.slice(0, 4) + ' ';
			card_number = card_number.slice(4);
		}
		return formatted_card_number;

	}

	/*getExpDate() {
		let now = new Date();
		let year = String(now.getFullYear() + 2);
		return `${("0" + (now.getMonth() + 1)).slice(-2)}/${year.slice(2, 4)}`
	}*/

	/*getCvv() {
		return Math.floor(100 + Math.random() * 900);
	}*/

	/*getCardNumber() {
		let rand_10 = Math.floor(1000000000 + Math.random() * 9000000000);
		let card_number = '534138' + rand_10;
		return card_number;
	}*/

	detectPinChange(value) {
		this.cdRef.detectChanges();
		this.mpin = this.globals.detectChange(value, this.mpin, 6);
	}

	detectAmountChange(value) {
		this.cdRef.detectChanges();
		this.amount = this.globals.detectChange(value, this.amount, 6);
	}



}
