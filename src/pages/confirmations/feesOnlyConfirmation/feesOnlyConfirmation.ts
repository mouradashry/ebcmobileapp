import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ApiService } from '../../../app/services/api.service';
import { GlobalVariablesService } from '../../../app/services/global-variables.service';
import { SuccessMessage } from '../../success-message/success-message';
import { FailureMessage } from '../../failure-message/failure-message';

/**
 * Generated class for the BillPayment page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
    selector: 'page-feesOnlyConfirmation',
    templateUrl: 'feesOnlyConfirmation.html',
})
export class FeesOnlyConfirmation {

    url: string = '/transfer/otp';
    amount = 0;
    fees = 0;
    dataSub: any;
    walletId: string;
    mpin: string;
    title = '';

    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public service: ApiService,
        public globals: GlobalVariablesService,
        private cdRef: ChangeDetectorRef) {

        this.amount = this.navParams.get('amount');
        this.fees = this.navParams.get('fees');
        this.title = this.navParams.get('title') || this.title;
    }

    async generateOtp() {

        this.globals.presentLoading();
        console.log(this.title);

        let res = await this.service.postData(this.url, { amount: this.amount, mpin: this.mpin }).toPromise();

        if (res.status == 200) {
            return this.navCtrl.setRoot(SuccessMessage, { message: res.data.message });
        }
        else
            return this.navCtrl.push(FailureMessage, { message: res.data.error });
    }

    ngOnDestroy() {
        if (this.dataSub)
            this.dataSub.unsubscribe();
    }

    detectPinChange(value) {
        this.cdRef.detectChanges();
        this.mpin = this.globals.detectChange(value, this.mpin, 6);
    }

    back() {
        this.navCtrl.pop();
    }

}