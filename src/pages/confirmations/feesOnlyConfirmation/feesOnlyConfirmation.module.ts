import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { FeesOnlyConfirmation } from './feesOnlyConfirmation';

@NgModule({
    declarations: [
        FeesOnlyConfirmation,
    ],
    imports: [
        IonicPageModule.forChild(FeesOnlyConfirmation),
        TranslateModule.forChild()
    ],
    exports: [
        FeesOnlyConfirmation
    ]
})
export class FeesOnlyConfirmationModule { }
