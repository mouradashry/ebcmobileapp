import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TransferConfirmation } from './transfer-confirmation';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    TransferConfirmation,
  ],
  imports: [
    IonicPageModule.forChild(TransferConfirmation),
    TranslateModule.forChild()
  ],
  exports: [
    TransferConfirmation
  ]
})
export class TransferConfirmationModule {}
