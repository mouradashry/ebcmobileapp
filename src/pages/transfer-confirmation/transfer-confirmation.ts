import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SuccessMessage } from '../success-message/success-message';
import { FailureMessage } from '../failure-message/failure-message';
import { ApiService } from '../../app/services/api.service'
import { GlobalVariablesService } from '../../app/services/global-variables.service'
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-transfer-confirmation',
  templateUrl: 'transfer-confirmation.html',
})
export class TransferConfirmation {
  amount: string;
  pin: string;
  fees: string;
  commissions: string;
  balance: string;
  request_ref_num: string;
  dataSub: any;
  walletId: string;
  additionalData: any;
  totalAmount: string;
  tips : any

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public service: ApiService, public globals: GlobalVariablesService
    , private cdRef: ChangeDetectorRef) {
    this.amount = this.navParams.get("amount");
    this.fees = this.navParams.get("fees");
    this.commissions = this.navParams.get("commissions");
    this.balance = this.navParams.get("new_balance");
    this.walletId = this.navParams.get("walletId");
    this.request_ref_num = this.navParams.get("request_ref_num");
    this.additionalData = this.navParams.get("additionalData");
    this.totalAmount = this.navParams.get("totalAmount");
  }

  valid() {
    if (!this.pin) {
      this.globals.presentToast('toast.3', 3);
      return false;
    }
    if (this.pin.length !== 6) {
      this.globals.presentToast('toast.2', 3);
      return false;
    }
    return true;
  }

  async confirm() {
    this.globals.isDisplayingR2p = false

    if (!this.valid())
      return;

    let url = '/transaction/completeTransaction';

    this.globals.presentLoading();
    let res = await this.service.postData(url, { mpin: this.pin, requestRefNum: this.request_ref_num }).toPromise();
    this.globals.dismissLoading();

    if (res.status == 200) {
      await this.globals.update('balance', this.balance);
      return this.navCtrl.setRoot(SuccessMessage, { message: res.data.message });
    }

    if (res.status == 400) {

      if (res.data.wrongMpin || res.data.endTrx == false) {
        return this.globals.presentToast(res.data.error);
      }

      return this.navCtrl.setRoot(FailureMessage, { message: res.data.error });
    }
  }

  home() {
    this.navCtrl.push(HomePage);
  }

  back() {
    this.navCtrl.pop();
  }

  detectPinChange(value) {
    this.cdRef.detectChanges();
    this.pin = this.globals.detectChange(value, this.pin, 6);
  }

  ngOnDestroy() {
    if (this.dataSub)
      this.dataSub.unsubscribe();
  }

  viewTotalAmount(totalAmount: number) {
    return totalAmount < 0 ? `+${(0 - totalAmount)}` : totalAmount;
  }

}