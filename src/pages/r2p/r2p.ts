import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { FailureMessage } from '../failure-message/failure-message';
import { TransferConfirmation } from '../transfer-confirmation/transfer-confirmation';
import { ApiService } from '../../app/services/api.service';
import { GlobalVariablesService } from '../../app/services/global-variables.service';
import { HomePage } from '../home/home';
import { TrxStep1Service } from '../../app/services/trx-step1.service';
import { QueueService } from '../../app/services/queue.service';

@Component({
  selector: 'page-r2p',
  templateUrl: 'r2p.html',
})
export class R2p {
  amount: string;
  walletId: string;
  deleteNotificationUrl: string = '/deleteNotifications';
  description: string;
  title: string = 'Request to Pay';
  disableAmount: boolean = false;
  refNum: string;
  additionalData: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public alertCtrl: AlertController, public service: ApiService,
    public globals: GlobalVariablesService, private cdRef: ChangeDetectorRef,
    private trxStep1Service: TrxStep1Service) {
    this.getInputParams();
  }


  async ngOnInit() {
    await this.deleteNotification();
  }

  async validate() {

    if (this.walletId) {
      if (!(this.walletId.length == 11 || this.walletId.length == 9))
        return this.globals.presentToast('toast.14');
    }
    else
      return this.globals.presentToast('toast.15');

    if (!this.amount)
      return this.globals.presentToast("toast.7");
    return await this.transfer();

  }

  async transfer() {

    let data = {
      amount: this.amount,
      recieverMobile: this.walletId,
      additionalData: this.additionalData
    }

    let res = await this.trxStep1Service.trxStep1(data);
    if (res[0])
      return await this.navCtrl.push(TransferConfirmation, res[1]);
    await this.navCtrl.setRoot(FailureMessage, res[1]);
  }

  getInputParams() {
    let notes = this.navParams.get('notes')
    this.walletId = this.navParams.get('merchantWalletId');
    this.refNum = this.navParams.get('refNum');
    let qrData = this.navParams.get('qrcodeData');

    if (typeof (qrData) === 'string')
      qrData = JSON.parse(qrData);
    this.amount = qrData.amount;
    this.description = `Request to pay with amount ${this.amount} EGP from wallet ${this.walletId}`;

    if (notes && notes != -1)
      this.description += ` notes: ${notes}`;



    if (qrData.additionalData) {


      this.additionalData = {
        billNumber: qrData.additionalData.billNumber == -1 ? '' : qrData.additionalData.billNumber,
        referenceLabel: qrData.additionalData.referenceLabel == -1 ? '' : qrData.additionalData.referenceLabel,
        customerLabel: qrData.additionalData.customerLabel == -1 ? '' : qrData.additionalData.customerLabel,
        terminalLabel: qrData.additionalData.terminalLabel == -1 ? '' : qrData.additionalData.terminalLabel,
        storeLabel: qrData.additionalData.storeLabel == -1 ? '' : qrData.additionalData.storeLabel,
        mobileNumber: qrData.additionalData.mobileNumber == -1 ? '' : qrData.additionalData.mobileNumber,
        purposeOfTransaction: qrData.additionalData.purposeOfTransaction == -1 ? '' : qrData.additionalData.purposeOfTransaction,
        loyaltyNumber: qrData.additionalData.loyaltyNumber == -1 ? '' : qrData.additionalData.loyaltyNumber,

        convenience: qrData.convenience == -1 ? '' : qrData.convenience,
        tips: null,
        tip: qrData.tip
      };

    }

    if (this.amount) this.disableAmount = true;
  }

  async cancel() {
    this.globals.isDisplayingR2p = false
    await this.navCtrl.setRoot(HomePage);
  }

  async deleteNotification() {
    await this.service.postData(this.deleteNotificationUrl, { id: this.navParams.get('notification_id') }, true, false).toPromise();
    await this.service.updateLast4requests();
  }

  detectNumberChange(value) {
    this.cdRef.detectChanges();
    this.walletId = this.globals.detectChange(value, this.walletId, 11);
  }

  detectAmountChange(value) {
    this.cdRef.detectChanges();
    this.amount = this.globals.detectChange(value, this.amount, 6);
  }

  detectTipsChange(value) {
    this.cdRef.detectChanges();
    this.additionalData.tips = this.globals.detectChange(value, this.additionalData.tips, 6);
  }


}