import { Component } from '@angular/core';
import { App,NavController, } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Splash } from '../../pages/splash/splash';
import { GlobalVariablesService } from '../../app/services/global-variables.service'
import { ApiService } from '../../app/services/api.service'

@Component({
  selector: 'page-log-out',
  templateUrl: 'logout.html',
})
export class LogOutPage {

  constructor(public navCtrl: NavController,
    private globals:GlobalVariablesService,public api:ApiService) {}

  async ionViewDidLoad() {
    if(this.globals.apiToken)
      await this.deleteDeviceToken();
    await this.deleteStorage();
  }

  //remove device token from backend DB
  async deleteDeviceToken(){
    let body = {
      device_token:null
    }
    let refreshDeviceTokenSubUrl = '/wallet_users/updateToken';
    let apiReq = await this.api.postData(refreshDeviceTokenSubUrl,body,true,false).toPromise();
  }

  async deleteStorage(){  
    await this.globals.delete([
      'balance','apiToken','walletId','last_4_requests','user_type']);
    this.navigate();    
  }

  navigate(){
    this.navCtrl.setRoot(Splash);
  }

}
