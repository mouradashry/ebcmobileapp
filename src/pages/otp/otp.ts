import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FailureMessage } from '../failure-message/failure-message';
import { SuccessMessage } from '../success-message/success-message';
import { ApiService } from '../../app/services/api.service';
import { GlobalVariablesService } from '../../app/services/global-variables.service';
import { FeesOnlyConfirmation } from '../confirmations/feesOnlyConfirmation/feesOnlyConfirmation';

@Component({
  selector: 'page-otp',
  templateUrl: 'otp.html',
})
export class Otp {
  amount: string;
  dataSub: any;
  walletId: string;
  url: string = '/transfer/initializeOtp';

  constructor(public navCtrl: NavController, public service: ApiService,
    public globals: GlobalVariablesService, private cdRef: ChangeDetectorRef) { }

  async validate() {
    if (this.amount === '0')
      return this.globals.presentToast("toast.13");
    await this.generateOtp();
  }

  async generateOtp() {

    this.globals.presentLoading();
    let res = await this.service.postData(this.url, { amount: this.amount }).toPromise();

    if (res.status == 200) {
      let data = res.data.message;
      return this.navCtrl.push(FeesOnlyConfirmation, { amount: data.otpAmount, fees: data.fees, title: 'OTP confirmation' });
    }
    else
      return this.navCtrl.push(FailureMessage, { message: res.data.error });

  }

  back() {
    this.navCtrl.pop();
  }

  detectAmountChange(value) {
    this.cdRef.detectChanges();
    this.amount = this.globals.detectChange(value, this.amount, 6);
  }

}