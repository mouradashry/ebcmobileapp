import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ApiService } from '../../app/services/api.service';
import { GlobalVariablesService } from '../../app/services/global-variables.service';
import { ServicePage } from '../service/service';
import { HomePage } from '../home/home';

/**
 * Generated class for the ServiceProviderPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-service-provider',
  templateUrl: 'service-provider.html',
})
export class ServiceProvider {
  cat_id: number;
  cat_name: string = "Service Provider"
  loadProvider: any;
  providers: Array<any>;


  constructor(public navCtrl: NavController, public navParams: NavParams, public api: ApiService,
    public global: GlobalVariablesService) {

    this.cat_id = navParams.get('id');
    this.cat_name = navParams.get('name');
  }

  async ionViewDidLoad() {
    this.global.presentLoading();
    let res = await this.api.postData('/serviceProvider_category', { category_id: this.cat_id }).toPromise();
    if (res.status === 200)
      this.providers = res.data.Service_providers;

    if (res.status === 400) {
      this.global.presentToast(res.data.error);
      this.back();
    }

  }

  home() {
    this.navCtrl.setRoot(HomePage);
  }

  back() {
    this.navCtrl.pop();
  }

  goToService(provider: any) {
    this.navCtrl.push(ServicePage, { id: provider.id, name: provider.name, cat_name: this.cat_name });
  }

}

