import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SuccessMessage } from './success-message';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
	declarations: [
		SuccessMessage,
	],
	imports: [
		IonicPageModule.forChild(SuccessMessage),
		TranslateModule.forChild()

	],
	exports: [
		SuccessMessage
	]
})
export class SuccessMessageModule { }
