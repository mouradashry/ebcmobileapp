import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';

/**
 * Generated class for the SuccessMessage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-success-message',
  templateUrl: 'success-message.html',
})
export class SuccessMessage {

  message: string = "";
  destination: string;
  iconType: string = 'success';

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.message = this.navParams.get("message");
    this.iconType = this.navParams.get('iconType') || this.iconType;
  }

  done() {

    this.navCtrl.setRoot(HomePage);
  }

}
