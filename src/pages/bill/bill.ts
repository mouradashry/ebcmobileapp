import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ApiService } from '../../app/services/api.service';
import { BilldetailsPage } from '../billdetails/billdetails';
import { GlobalVariablesService } from '../../app/services/global-variables.service';
import { HomePage } from '../home/home';


@Component({
  selector: 'page-bill',
  templateUrl: 'bill.html',
})
export class BillPage {

  provider_id: number;
  service_name: string;
  service_type: any;
  aggregator_id: number;
  service_id: number;
  recipient;
  recipientMsg;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiService,
    public global: GlobalVariablesService) {

    this.service_name = navParams.get("service_name");
    this.service_type = navParams.get("service_type");
    this.aggregator_id = navParams.get("aggregator_id");
    this.provider_id = navParams.get("provider_id");
    this.service_id = navParams.get("service_id");
  }

  home() {
    this.navCtrl.setRoot(HomePage);
  }

  back() {
    this.navCtrl.pop();
  }

  async billDetails() {
    this.recipientMsg = '';

    if (!this.recipient) {
      this.recipientMsg = "Recipient is required";
      return;
    }

    let body = {
      service_type: this.service_type,
      trx_type: await this.global.getTrxType(),
      recipient: this.recipient,
      service_id: this.service_id,
      aggregator_id: this.aggregator_id,
      provider_id: this.provider_id,
    };

    this.global.presentLoading();
    let res = await this.api.postData('/billenquiry_or_Voucher', body).toPromise();

    if (res.status === 200) {
      let result = res.data.finalResult;

      this.navCtrl.push(BilldetailsPage,
        {
          RefNum: result.requestRefNum,
          totalAmount: result.totalAmount,
          totalFees: result.totalFees,
          amount: result.amount,
          newBalance: result.newBalance,
          convenience: result.convenience,
          recipient: this.recipient,
          commissions: result.commissions
        });
    }

    if (res.status !== 200) {
      this.global.presentToast(res.data.message);
      this.back();
    }

  }
}
