import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalVariablesService } from '../../app/services/global-variables.service'
import { ApiService } from '../../app/services/api.service'
import { Validator } from '../../app/services/validator.service'

/**
 * Generated class for the VccEnquireComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@IonicPage()
@Component({
  selector: 'vcc-enquire',
  templateUrl: 'vcc-enquire.html',
})
export class VccEnquire {

  mpin: string;
  step: number = 1;
  VccCards: any[] = [];
  validMpin: boolean = false;
  VccDetails: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private cdRef: ChangeDetectorRef,
    public globals: GlobalVariablesService,
    private api: ApiService,
    private valid: Validator) {

  }

  back() {
    this.navCtrl.pop();
  }


  detectPinChange(value) {
    this.cdRef.detectChanges();
    this.mpin = this.globals.detectChange(value, this.mpin, 6);
  }

  validateMpin() {
    this.validMpin = this.valid.isNull(this.mpin);
    if (this.validMpin)
      this.globals.presentToast("Mpin Field Is Required");
    else
      this.toStep2();
  }
  async toStep2() {
    this.globals.presentLoading();
    let response = await this.api.postData('/transfer/vcc/listUserVcc', { type: "VCCIssuance", mpin: this.mpin }).toPromise();
    if (response.data.message) {
      this.globals.presentToast(response.data.message);
    }
    else {
      this.VccCards = response.data.userVCCS;
      this.step = 2;
    }
  }

  async cardDetails(refNum) {
    this.globals.presentLoading();
    let response = await this.api.postData('/transfer/vcc/inquireVcc', { originalRequestId: refNum }).toPromise();
    if (response.data.message) {
      this.globals.presentToast(response.data.message);
    }
    else {
      this.VccDetails = response.data.card
      this.step = 3;
    }

  }

}

