import { NgModule } from '@angular/core';
import { IonicModule, IonicPageModule } from 'ionic-angular';
import { VccEnquire } from './vcc-enquire';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    VccEnquire,
  ],
  imports: [
    IonicPageModule.forChild(VccEnquire),
    TranslateModule.forChild(),
    IonicModule,
  ],
  exports: [
    VccEnquire
  ]
})
export class VccEnquireComponentModule { }
