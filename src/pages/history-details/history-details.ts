import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TransactionDetails } from '../transaction-details/transaction-details';
import { GlobalVariablesService } from '../../app/services/global-variables.service';
import { ApiService } from '../../app/services/api.service';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-history-details',
  templateUrl: 'history-details.html',
  
})
export class HistoryDetails {
  requests = new Array();
  allHistory = new Array();
   count = 0; // use in pagination
  show: boolean = true;
  subUrl = '/wallet_users/history';
  dataSub: any;
  walletId: string;
  select: 'sender' | 'reciever';

  constructor(public navCtrl: NavController, public params: NavParams,
    private globals: GlobalVariablesService, private api: ApiService) {
  }

  /*
  *params:type='sender' or 'reciever'
  */
  senderOrReceiver(request, type): boolean {
    return request[`${type}_identifier`] === this.walletId;
  }

  home() {
    this.navCtrl.setRoot(HomePage);
  }

  back() {
    this.navCtrl.pop();
  }

  ngAfterViewInit() {
    this.showAll()
  }
  async ionViewDidLoad() { 
    this.walletId = await this.globals.get('walletId');
    await this.viewHistory();
  }
  // async ngOnInit() {
  //   this.walletId = await this.globals.get('walletId');
  //   await this.viewHistory();

  // }

  //pagination

  doInfinite(infiniteScroll) {
   
    console.log('Begin async operation');
    this.count+=10;
    console.log("counter = "+this.count);
    
            
    setTimeout(() => {
      for (let i = this.count; i < this.count+10; i++) {
        if(this.allHistory[i])
          this.requests.push( this.allHistory[i] );
      }

      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 500);
    
 
  }

 

  viewSingleTxDetials(req: any) {
    this.navCtrl.push(TransactionDetails, { request: req });
  }

  hideSender() {
    this.select = 'reciever';
  }

  hideReceiver() {
    this.select = 'sender';
  }

  showAll() {
    this.select = null;
  }

  viewHistory() {
    this.globals.presentLoading();
    this.dataSub = this.api.postData(this.subUrl, {})
      .subscribe(res => {
        if (res.status == 200) {
          this.allHistory = res.data.requests;
          console.log("history length >>>>>>>>>>> " + this.allHistory.length);
          if(res.data.requests.length < 10){
            for (let i = 0; i < res.data.requests.length; i++) {
              this.requests.push(this.allHistory[i])
              // this.requests[i]=9
              console.log("requests >>>" + this.allHistory[0]);
  
            }
          }
          else{
            for (let i = 0; i < 10; i++) {
              this.requests.push(this.allHistory[i])
              // this.requests[i]=9
              console.log("requests >>>" + this.allHistory[0]);
  
            }
          }


        }
        else if (res.status == 400)
          this.globals.presentToast(res.data.message, 2);

      });

  }

  ngOnDestroy() {
    this.dataSub.unsubscribe();
  }

}
