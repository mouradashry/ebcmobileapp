import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { GlobalVariablesService } from '../../app/services/global-variables.service'
import { ApiService } from '../../app/services/api.service'
import { SuccessMessage } from '../success-message/success-message'
import { FailureMessage } from '../failure-message/failure-message'
import { FavouriteService } from '../../app/services/favourite.service';
import { ContactsService } from '../../app/services/contacts.service';

@IonicPage()
@Component({
  selector: 'page-cash-r2p',
  templateUrl: 'cash-r2p.html',
})
export class CashR2p {
  mobile_numebr: string;
  amount: string;
  mpin: string;
  convenience: string;
  tip: boolean;
  reference1: string;
  reference2: string;
  mobileNumber: string;
  customerLabel: string;
  terminalLabel: string;
  loyaltyNumber: string;
  purposeOfTransaction: string;
  storeLabel: string;
  tipOrConvenience: 'tip' | 'convenience' | 'none' = 'none';
  title: string;
  description: string;
  pageType: string;
  r2pSuburl: string = '/transfer/startrequest2pay';
  cashoutSuburl: string = '/transfer/cashout';
  cashinSuburl: string = '/transfer/cashin';
  showMpin: boolean = true;

  constructor(
    public navCtrl: NavController, public navParams: NavParams,
    private cdRef: ChangeDetectorRef, public globals: GlobalVariablesService,
    private api: ApiService, public viewCtrl: ViewController,
    private favouriteService: FavouriteService,
    private contactsService: ContactsService,
  ) {
  }

  ionViewWillEnter() {
    this.pageType = this.navParams.get('type');
    this.setFavData();
    if (this.pageType.substring(0, 4) === 'cash') this.handleCash(this.pageType);
    else if (this.pageType === 'r2p') this.handleR2p();
  }

  validate() {

    if (this.mobile_numebr) {
      if (!(this.mobile_numebr.length == 11 || this.mobile_numebr.length == 9))
        return this.globals.presentToast('toast.5');
    }
    else
      return this.globals.presentToast('toast.6');


    if (!this.globals.isValidTip(this.tip))
      return;

    if (this.showMpin) {
      if (this.mpin) {
        if (this.mpin.length != 6)
          return this.globals.presentToast('toast.2');
      }
      else
        return this.globals.presentToast('toast.3');
    }
    if (!this.amount)
      return this.globals.presentToast("toast.7");

    if (this.tipOrConvenience === 'convenience' &&
      (!this.convenience || this.convenience === ''))
      return this.globals.presentToast('toast.8');

    return this.preSubmit();

  }

  handleCash(type) {

    if (type !== 'cashOut') {
      this.title = 'Cash in';
      this.description = "Deposit money into consumer's wallet";
      return;
    }

    this.title = 'Cash out';
    this.description = "Withdraw money from consumer's wallet";
  }

  handleR2p() {
    this.title = 'Request to Pay';
    this.description = "Request an amount of money from a mobile number";
    this.showMpin = false;
  }

  preSubmit() {
    let body = {
      Mnumber: this.mobile_numebr,
      Money: this.amount,
      MpinCode: this.mpin,
    }
    let subUrl;
    if (this.pageType === 'r2p') {
      subUrl = this.r2pSuburl;
      if (this.tipOrConvenience === 'tip' || this.tip)
        body['tip'] = true;

      if (this.tipOrConvenience === 'convenience')
        body['convenience'] = this.convenience;

      body['billNmuber'] = this.reference1;
      body['referenceLabel'] = this.reference2;
      body['storeLabel'] = this.storeLabel;
      body['terminalLabel'] = this.terminalLabel;
      body['customerLabel'] = this.customerLabel;
      body['mobileNumber'] = this.mobileNumber;
      body['loyaltyNumber'] = this.loyaltyNumber;
      body['purposeOfTransaction'] = this.purposeOfTransaction;
    }
    else
      subUrl = this.pageType === 'cashOut' ? this.cashoutSuburl : this.cashinSuburl;
    return this.submit(body, subUrl);
  }

  submit(body, subUrl) {
    this.globals.presentLoading();
    this.api.postData(subUrl, body)
      .subscribe(res => {
        if (res.status == 200) {
          let navParamData = { message: res.data.message };

          // in case the request was cashin or cashout send the iconType pending
          if (this.pageType !== 'r2p')
            navParamData['iconType'] = 'success';

          return this.navCtrl.push(SuccessMessage, navParamData);
        }
        if (res.status == 400)
          return this.navCtrl.push(FailureMessage, { message: res.data.message });
      });
  }

  async favourite() {
    let data = {
      title: this.title,
      trx_type: this.pageType,
      receiver: this.mobile_numebr,
      amount: this.amount,
      service_type: "transaction",
      data: this.pageType === 'r2p' ? {
        reference1: this.reference1,
        reference2: this.reference2,
        tip: this.tip,
        convenience: this.convenience,
        tipOrConvenience: this.tipOrConvenience
      } : null
    }
    await this.favouriteService.addandNameFavourite(data);
  }

  setFavData() {
    let favouriteData = this.navParams.get('favourite');
    if (favouriteData) {
      this.mobile_numebr = favouriteData.receiver;
      this.pageType = favouriteData.trx_type;
      this.amount = favouriteData.amount;
      let r2pData = favouriteData.data;
      if (r2pData) {
        this.tipOrConvenience = r2pData.tipOrConvenience;
        this.tip = r2pData.tip;
        this.reference1 = r2pData.reference1;
        this.reference2 = r2pData.reference2;
        this.convenience = r2pData.convenience;
      }

    }
  }

  getContact() {
    this.contactsService.getContact(this, 'mobile_numebr');

  }

  back() {
    this.navCtrl.pop();
  }


  detectNumberChange(value) {
    this.cdRef.detectChanges();
    this.mobile_numebr = this.globals.detectChange(value, this.mobile_numebr, 11);
  }

  detectAmountChange(value) {
    this.cdRef.detectChanges();
    this.amount = this.globals.detectChange(value, this.amount, 6);
  }

  detectPinChange(value) {
    this.cdRef.detectChanges();
    this.mpin = this.globals.detectChange(value, this.mpin, 6);
  }

  detectConvenienceChange(value) {
    this.cdRef.detectChanges();
    this.convenience = this.globals.detectChange(value, this.convenience, 6);
  }



}