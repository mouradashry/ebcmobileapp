import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CashR2p } from './cash-r2p';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    CashR2p,
  ],
  imports: [
    IonicPageModule.forChild(CashR2p),
    TranslateModule.forChild()
  ],
  exports: [
    CashR2p
  ]
})
export class CashR2pModule {}
