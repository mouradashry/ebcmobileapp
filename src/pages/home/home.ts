import { Component } from '@angular/core';
import { NavController, Platform, NavParams, Events, App, AlertController, Item } from 'ionic-angular';
import { GlobalVariablesService } from './../../app/services/global-variables.service';
import { ApiService } from './../../app/services/api.service';
import { Favourites } from '../favourites/favourites';
import { HistoryDetails } from '../history-details/history-details';
import { Category } from '../category/category';
import { MoneyTransfer } from '../money-transfer/money-transfer';
import { Cash } from '../cash/cash';
import { TransactionDetails } from '../transaction-details/transaction-details';
import { CashR2p } from '../cash-r2p/cash-r2p';
import { R2p } from '../r2p/r2p';
import { Otp } from '../otp/otp';
import { Purchase } from '../purchase/purchase';
import { NotificationsPage } from '../notifications/notifications';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatisticsPage } from '../statistics/statistics';
import { ScanQrcodeService } from './../../app/services/scanQrcode.service';
import { OnlineCardsPage } from '../online-cards/online-cards';
import { Settings } from '../settings/settings';
import { StaticQrCode } from '../qrcode/static-qrcode/static-qrcode';
import { listCards } from '../cardManagment/listCards/listCards';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  balance: number;
  userType: string;
  stop: boolean = false;
  getbalanceUrl: string = '/wallet_users/getbalance';
  turnToViewUrl = '/updateNotification'
  getTrxDetails = '/transaction/details';
  last4Requests: any = {};
  last4RequestKeys: any = [];
  waletId: string;
  isChild: boolean = false;

  constructor(public navAlert: AlertController, public navCtrl: NavController, public globals: GlobalVariablesService,
    public api: ApiService, public platform: Platform, private params: NavParams,
    public events: Events, public app: App,
    public splashScreen: SplashScreen, private scanqr: ScanQrcodeService, ) {
    this.platform.resume.subscribe(() => {
      console.log("ready")
      this.updateRequests();
    });
  }

  ionViewWillEnter() {
    this.getWalletId();
    this.getBalance();
    this.updateRequests();
    this.getUserType();
  }

  async getWalletId() {
   this.waletId  = await this.globals.get('walletId');
   if(this.waletId.length === 9) this.isChild = true
  }

  ionViewDidEnter() {
    //hide native splash when statring app
    let hideSplash = this.params.get('hideSplash');
    if (hideSplash) { this.splashScreen.hide(); }
  }

  /* async getRequests(){
     let requests = await this.globals.get('last_4_requests')
     if(!requests) await this.updateRequests()
   }*/

  async getBalance() {
    let res = await this.api.postData(this.getbalanceUrl, {}, true, false).toPromise();
    if (res.status == 200)
      return await this.globals.update('balance', res.data.balance);
    await this.globals.get('balance');
  }

  async getUserType() {
    this.userType = await this.globals.get('user_type');
  }

  async updateRequests() {
    this.last4Requests = await this.api.updateLast4requests();
    this.last4RequestKeys = Object.keys(this.last4Requests);
  }

  async acceptOrReject(notification, refNum: string) {


    await this.turnNotificationToViewed(refNum, notification);

    if (notification.data.r2p) {
      notification.data['notification_id'] = notification.id;
      this.globals.isDisplayingR2p = true;
      return this.navCtrl.setRoot(R2p, notification.data);
    }

    this.navCtrl.setRoot(Cash, { cash: notification.data });
  }

  /**
   * @description function to change the notification state to viewed
   * @param refNum
   */
  async turnNotificationToViewed(refNum: string, notification: any) {
    let last4Requests = this.last4Requests;
    alert(notification)
    if (!last4Requests[refNum])
      return false;
   // last4Requests[refNum].viewed = true;
    await this.globals.update(this.globals.LAST4Requests, last4Requests);
    await this.api.postData(this.turnToViewUrl, { id: notification.id }).toPromise()

    return true;
  }

  /**
   * @description function to return the viewed state of a notification
   * @param refNum
   */
   isNotificationViewed(refNum: string) {
    let last4Requests = this.last4Requests;

    if (!last4Requests[refNum])
      return false;

    let request = last4Requests[refNum];

    if (request.viewed)
      return true;

    return false
    //  return last4Requests[refNum].viewed === true;
  }

  async viewRequest(trxId) {
    await this.globals.presentLoading();
    let res = await this.api.postData(this.getTrxDetails, { trxRefNum: trxId }).toPromise();
    if (res.status !== 200)
      return this.globals.presentToast(res.data.error);

    let request = res.data;
    this.navCtrl.push(TransactionDetails, { request: request })
  }

  moneyTransfers() {
    console.log('click on transfer');

    this.navCtrl.push(MoneyTransfer);
  }

  purchases() {
    console.log('click on purchase');

    this.navCtrl.push(Purchase);
  }

  bill() {
    this.navCtrl.push(Category);
  }

  history() {
    this.navCtrl.push(HistoryDetails);
  }

  favourites() {
    this.navCtrl.push(Favourites);
  }

  qrcode() {
    this.navCtrl.push(StaticQrCode);
  }

  cash(type) {
    this.navCtrl.push(CashR2p, { type: type });
  }

  r2p() {
    this.navCtrl.push(CashR2p, { type: 'r2p' });
  }

  otp() {
    this.navCtrl.push(Otp);
  }

  nfc() {
    //this.navCtrl.push(Nfc);
    this.navCtrl.push(StatisticsPage);
  }

  notifications() {
    this.navCtrl.push(NotificationsPage);
  }

  onlineCards() {
    this.navCtrl.push(OnlineCardsPage);
  }

  setting() {
    this.navCtrl.push(Settings);
  }

  loadWallet() {
    this.navCtrl.push(listCards);
  }

  async scanQrcode() {
    let data = await this.scanqr.scan();
    if (data)
      await this.navCtrl.push(Purchase, { qrcode: data });
  }

}
