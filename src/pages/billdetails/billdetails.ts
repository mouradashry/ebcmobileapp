import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ApiService } from '../../app/services/api.service';
import { SuccessMessage } from '../success-message/success-message';
import { GlobalVariablesService } from '../../app/services/global-variables.service';
import { FailureMessage } from '../failure-message/failure-message';
import { HomePage } from '../home/home';


@Component({
  selector: 'page-billdetails',
  templateUrl: 'billdetails.html',
})
export class BilldetailsPage {

  RefNum;
  totalAmount;
  totalFees;
  amount;
  newBalance;
  convenience;
  recipient;
  commissions;
  mpin: string;
  mpinMsg;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiService,
    public global: GlobalVariablesService,
    private cdRef: ChangeDetectorRef) {

    this.RefNum = navParams.get("RefNum");
    this.totalAmount = navParams.get("totalAmount");
    this.totalFees = navParams.get("totalFees");
    this.amount = navParams.get("amount");
    this.newBalance = navParams.get("newBalance");
    this.convenience = navParams.get("convenience");
    this.recipient = navParams.get("recipient");
    this.commissions = navParams.get("commissions");

  }

  home() {
    this.navCtrl.setRoot(HomePage);
  }

  back() {
    this.navCtrl.pop();
  }

  async confirm() {
    this.mpinMsg = ''

    if (!this.mpin) {
      this.mpinMsg = "mpin is required"
      return;
    }

    let paymentRequest = { trxRef: this.RefNum, mpin: this.mpin };
    this.global.presentLoading();
    let payment = await this.api.postData('/servicepayment', paymentRequest).toPromise();

    if (payment.status === 200) {
      this.navCtrl.push(SuccessMessage, { message: payment.data.finalResult.message });
    }
    if (payment.status !== 200) {
      this.navCtrl.push(FailureMessage, { message: payment.data.message });
    }
  }

  detectPinChange(value) {
    this.cdRef.detectChanges();
    this.mpin = value.length > 6 ? value.substring(0, 6) : value;
  }
  viewTotalAmount(totalAmount: number) {
    return totalAmount < 0 ? `+${(0 - totalAmount)}` : totalAmount;
  }

}