import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { FailureMessage } from '../failure-message/failure-message';
import { TransferConfirmation } from '../transfer-confirmation/transfer-confirmation';
import { ApiService } from '../../app/services/api.service';
import { GlobalVariablesService } from '../../app/services/global-variables.service';
import { FavouriteService } from '../../app/services/favourite.service';
import { HomePage } from '../home/home';
import { ContactsService } from '../../app/services/contacts.service';

@IonicPage()
@Component({
  selector: 'page-money-transfer',
  templateUrl: 'money-transfer.html',
})
export class MoneyTransfer {
  amount: string;
  dataSub: any;
  walletId: string;
  url: string = '/transaction/checkSender';
  title: string = 'Transfer';
  description: string;
  transType = 'p2p';

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public alertCtrl: AlertController, public service: ApiService,
    public globals: GlobalVariablesService, private cdRef: ChangeDetectorRef,
    private barcode: BarcodeScanner,
    private favouriteService: FavouriteService,
    private contactsService: ContactsService) {
    this.getIncomingData();

    //this.description = 'Transfer any amount from 5 to 3000 L.E to any Wallet ID';

  }


  async validate() {
    let user_walletId = await this.globals.get('walletId');
    if (this.walletId) {
      if (this.walletId.length != 11)
        return this.globals.presentToast('toast.5');
      if (this.walletId === user_walletId)
        return this.globals.presentToast('toast.12');
    }
    else
      return this.globals.presentToast('toast.6');

    if (!this.amount)
      return this.globals.presentToast("toast.7");
    return await this.transfer();

  }

  async transfer() {
    this.globals.presentLoading();
    this.dataSub = this.service.postData(this.url, {
      amount: this.amount,
      recieverMobile: this.walletId,
      type: this.transType
    })
      .subscribe(res => {
        this.globals.dismissLoading();
        if (res.status == 200) {
          this.navCtrl.push(TransferConfirmation, {
            amount: this.amount,
            fees: res.data.senderOk.totalFees,
            commissions: res.data.senderOk.totalComms,
            new_balance: res.data.senderOk.newBalance,
            walletId: this.walletId,
            request_ref_num: res.data.requestRefNum,
            totalAmount: res.data.senderOk.totalAmount
          });
        }
        else
          this.navCtrl.setRoot(FailureMessage, { message: res.data.error });
      });
  }

  async favourite() {
    let data = {
      title: this.title,
      trx_type: this.transType,
      receiver: this.walletId,
      amount: this.amount,
      service_type: "transaction"
    }
    await this.favouriteService.addandNameFavourite(data);
  }

  getIncomingData() {
    let favouriteData = this.navParams.get('favourite');
    if (favouriteData) {
      this.walletId = favouriteData.receiver;
      this.amount = favouriteData.amount;
    }

  }

  getContact() {
    this.contactsService.getContact(this, 'walletId');
  }

  home() {
    this.navCtrl.setRoot(HomePage);
  }

  back() {
    this.navCtrl.pop();
  }


  detectNumberChange(value) {
    this.cdRef.detectChanges();
    this.walletId = this.globals.detectChange(value, this.walletId, 11);
  }

  detectAmountChange(value) {
    this.cdRef.detectChanges();
    this.amount = this.globals.detectChange(value, this.amount, 6);
  }

  ngOnDestroy() {
    if (this.dataSub)
      this.dataSub.unsubscribe();
  }


}