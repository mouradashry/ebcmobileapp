import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MoneyTransfer } from './money-transfer';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    MoneyTransfer,
  ],
  imports: [
    IonicPageModule.forChild(MoneyTransfer),
    TranslateModule.forChild()
  ],
  exports: [
    MoneyTransfer
  ]
})
export class MoneyTransferModule {}