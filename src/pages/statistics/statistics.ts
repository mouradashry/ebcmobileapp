import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ApiService } from '../../app/services/api.service'
import { GlobalVariablesService } from '../../app/services/global-variables.service'


/**
 * Generated class for the StatisticsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-statistics',
  templateUrl: 'statistics.html',
})
export class StatisticsPage {
  statistics: any;
  url = '/merchant-statistics';

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private api: ApiService, private globals: GlobalVariablesService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StatisticsPage');
  }

  back() {
    this.navCtrl.pop();
  }

  async ngOnInit() {
    this.globals.presentLoading()
    let res = await this.api.postData(this.url, {}).toPromise();
    console.log(Object.keys(res.data));

    if (res.status == 200)
      //depit is out , credit is in
      this.statistics = res.data;
    else if (res.status == 400)
      this.globals.presentToast(res.data.message);
  }

  home() {
    this.navCtrl.setRoot('HomePage');
  }

}
