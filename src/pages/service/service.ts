import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ApiService } from '../../app/services/api.service';
import { GlobalVariablesService } from '../../app/services/global-variables.service';
import { HomePage } from '../home/home';
import { VoucherPage } from '../voucher/voucher'
import { BillPage } from '../bill/bill';
@Component({
  selector: 'page-service',
  templateUrl: 'service.html',
})
export class ServicePage {
  provider_id: number;
  provider_name: string;
  services: Array<any>;
  constructor(public navCtrl: NavController, public navParams: NavParams, public api: ApiService,
    public global: GlobalVariablesService) {
    this.provider_id = navParams.get("id");
    this.provider_name = navParams.get("name");
  }

  async ionViewDidLoad() {
    this.global.presentLoading();
    let res = await this.api.postData("/listServices", { service_provider_id: this.provider_id }).toPromise();
    if (res.status === 200) {
      this.services = res.data.services
    }

    else if (res.status === 400) {
      this.global.presentToast(res.data.message);
      this.back();
    }
  }

  home() {
    this.navCtrl.setRoot(HomePage);
  }

  back() {
    this.navCtrl.pop();
  }

  async goToService(service) {

    let res = await this.api.postData("/serviceMangement/getService", { id: service.service_id }).toPromise()
    let selectedService = res.data.service
    let selectedServiceValueList: any
    let serviceValue: any;
    let serviceInquire: any

    if (selectedService && (selectedService.bill_inquiry_reference || selectedService.name.includes('inquiry')))
      serviceInquire = true

    if (selectedService.serviceValueList)
      selectedServiceValueList = selectedService.serviceValueList.split(";")

    if (selectedService.serviceValue)
      serviceValue = selectedService.serviceValue

    let object = {
      service_name: service.name,
      service_type: service.type,
      aggregator_id: service.aggregator_id,
      provider_id: service.service_provider_id,
      service_id: service.id,
      serviceValueList: selectedServiceValueList,
      serviceValue: serviceValue,
      serviceInquire: serviceInquire
    }


    //sent object must contain {  trx_type, recipient, service_id, amount }
    if (service.type == 'Bill')
      this.navCtrl.push(BillPage, object);
    if (service.type == 'Voucher')
    this.navCtrl.push(VoucherPage, object);
  }
}
