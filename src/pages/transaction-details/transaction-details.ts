import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GlobalVariablesService } from './../../app/services/global-variables.service';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-transaction-details',
  templateUrl: 'transaction-details.html',
})
export class TransactionDetails {
  request: any;
  walletId: string;
  is_sender: boolean;
  is_receiver: boolean;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private globals: GlobalVariablesService) {

    this.request = this.navParams.get("request");
    this.request.receiver_identifier = (this.request.receiver_identifier ||  this.request.reciever_identifier || '');
  }

  home() {
    this.navCtrl.push(HomePage);
  }

  back() {
    this.navCtrl.pop();
  }

  async ionViewDidLoad() {
    await this.getWalletId();
    this.feesCheck(this.request);
  }

  async getWalletId() {
    this.walletId = await this.globals.get('walletId');
  }


  feesCheck(request) {
    this.is_sender = request.sender_identifier == this.walletId;
    this.is_receiver = request.reciever_identifier == this.walletId;
  }
}
