import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams, App, ViewController } from 'ionic-angular';
import { CreateYourPin } from '../create-your-pin/create-your-pin';
import { ApiService } from '../../app/services/api.service'
import { GlobalVariablesService } from '../../app/services/global-variables.service'
import { AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-verification',
  templateUrl: 'verification.html',
})
export class Verification {

  code: string = "";
  dataSub: any;
  constructor(public navCtrl: NavController,
    public navParams: NavParams, private alertCtrl: AlertController,
    public service: ApiService, public globals: GlobalVariablesService,
    private cdRef: ChangeDetectorRef, public app: App, public view: ViewController) {
    this.view.showBackButton(false);
  }


  ionViewDidLoad() {

    this.globals.presentAlert('alert.verification.1', this.navParams.get('verification_code'));
  }

  validate() {
    if (this.code) {
      if (this.code.length != 4)
        return this.globals.presentToast('toast.17', 2);
      this.update();
    }
    else {
      this.globals.presentToast('toast.18', 2);
    }
  }

  update() {
    let url = '/wallet_users/validate';
    this.globals.presentLoading();
    this.dataSub = this.service.postData(url, {
      verification_code: this.code,
      mobile_number: this.navParams.get("walletId")
    }, false)
      .subscribe(res => {
        console.log(res);
        this.globals.dismissLoading();
        if (res.status == 200)
          this.navCtrl.push(CreateYourPin, { walletId: this.navParams.get("walletId") });

        if (res.status == 400)
          return this.globals.presentToast(res.data.error, 2);
      });

  }

  detectChange(value) {
    this.cdRef.detectChanges();
    this.code = this.globals.detectChange(value, this.code, 4);
  }

  ngOnDestroy() {
    if (this.dataSub)
      this.dataSub.unsubscribe();
  }

}
