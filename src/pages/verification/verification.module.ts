import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Verification } from './verification';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
	declarations: [
		Verification,
	],
	imports: [
		IonicPageModule.forChild(Verification),
		TranslateModule.forChild()

	],
	exports: [
		Verification
	]
})
export class VerificationModule { }
