import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ForgotMpinPage } from './forgot-mpin';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ForgotMpinPage,
  ],
  imports: [
    IonicPageModule.forChild(ForgotMpinPage),
    TranslateModule.forChild()
  ],
  exports: [
    ForgotMpinPage
  ]
})
export class ForgotMpinPageModule {}
