import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignupIntermediate } from './signup-intermediate';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    SignupIntermediate,
  ],
  imports: [
    IonicPageModule.forChild(SignupIntermediate),
    TranslateModule.forChild()
  ],
  exports: [
    SignupIntermediate
  ]
})
export class SignupIntermediateModule {}
