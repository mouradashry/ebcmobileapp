import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, App } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { Register } from '../register/register'
import { Verification } from '../verification/verification';
import { ApiService } from '../../app/services/api.service'
import { GlobalVariablesService } from '../../app/services/global-variables.service'

@IonicPage()
@Component({
  selector: 'page-signup-intermediate',
  templateUrl: 'signup-intermediate.html',
})
export class SignupIntermediate {

  walletId: string;
  loader: any;
  theme :string= this.globals.theme.split('')[0].toLocaleUpperCase() + this.globals.theme.slice(1,this.globals.theme.length) ; 
  src = `assets/${this.theme.toLowerCase()}/logo-splash.png`
  constructor(public navCtrl: NavController, public service: ApiService,
    public globals: GlobalVariablesService, public modlCtrl: ModalController,
    private cdRef: ChangeDetectorRef, private app: App) {
  }



  async validate() {
    if (this.walletId) {
      if (!(this.walletId.length == 11 || this.walletId.length == 9))
        return this.globals.presentToast('toast.1');
      await this.check();
    }
    else
      this.globals.presentToast('toast.16');

  }

  back() {
    this.navCtrl.pop();
  }

  async check() {
    let url = '/wallet_users/checkIfRegistered';
    this.globals.presentLoading();
    let res = await this.service.postData(url, { mobile_number: this.walletId, scheme_id: this.globals.appSchemeId }).toPromise();
    this.globals.dismissLoading();

    if (res.status == 200)
      return this.navCtrl.push(Verification, {
        walletId: this.walletId,
        verification_code: res.data.verification_code
      });

    if (res.status == 400) {
      //user doesn't exist (not registered)
      if (res.data.status === 'Unregistered') {
        let modal = this.modlCtrl.create(Register);
        modal.present();
      }

      //user already registered
      else
        this.globals.presentToast(res.data.message);
    }
  }

  //force user to enter valid mPin and mobile
  detectMobileChange(value) {
    this.cdRef.detectChanges();
    this.walletId = this.globals.detectChange(value, this.walletId, 11);
  }

}