import { Component, OnInit } from '@angular/core';
import { IonicPage, Platform } from 'ionic-angular';
import { ApiService } from '../../app/services/api.service';
import { GlobalVariablesService } from '../../app/services/global-variables.service';
import { NavController } from 'ionic-angular';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import { TranslationService } from '../../app/services/translation.service';

@IonicPage()
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
})
export class NotificationsPage implements OnInit {
  notifications: any[] = [];
  offset: number = 1; //init value
  orderBy = { asc: 0, desc: 1 };
  getUrl: string = '/getNotifications';
  noMoreData = false;
  showNoData = false;
  constructor(
    public navCtrl: NavController,
    private api: ApiService,
    private globals: GlobalVariablesService,
    public platform: Platform,
    public translate: TranslationService) {
  }

  back() {
    this.navCtrl.pop();
  }

  async ngOnInit() {
    this.globals.presentLoading();
    let notifications = await this.get();
    console.log('notifications', notifications);
    if (notifications)
      this.notifications = notifications;

    else if (this.noMoreData && this.offset == 1)
      this.showNoData = true;

  }

  async get() {
    let res = await this.api.postData(this.getUrl, { skip: this.offset }).toPromise();

    if (res.status == 200) {
      if (res.data.notifications.length == 0) {
        this.noMoreData = true;
        return false;
      }
      this.offset = res.data.skip;
      return res.data.notifications;
    }
    this.globals.presentToast(res.data.error);
    return false;
  }

  async fetchMoreData(infiniteScroll) {
    let newNotifications = await this.get();
    if (newNotifications)
      this.notifications = this.notifications.concat(newNotifications);

    if (this.noMoreData)
      infiniteScroll.enable(false);

    infiniteScroll.complete();
  }
}