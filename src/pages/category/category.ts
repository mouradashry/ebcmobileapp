import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from '../service-provider/service-provider';
import { ApiService } from '../../app/services/api.service';
import { GlobalVariablesService } from '../../app/services/global-variables.service';
import { HomePage } from '../home/home';


@Component({
  selector: 'page-category',
  templateUrl: 'category.html',
})
export class Category {

  category_name: string = 'Category';
  categories: Array<any>;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiService,
    public global: GlobalVariablesService) {
  }

  async ionViewDidLoad() {
    this.global.presentLoading();
    let res = await this.api.postData("/serviceMangement/Category/get_categories", {}).toPromise();

    if (res.status == 200) {
      this.categories = res.data.categories;
      return;
    }

    if (res.status !== 200) {
      this.global.presentToast(res.data.message);
      this.back();
    }

  }
  home() {
    this.navCtrl.setRoot(HomePage);
  }
  back() {
    this.navCtrl.pop();
  }

  goToCategory(category: any) {
    this.navCtrl.push(ServiceProvider, { id: category.id, name: category.name });
  }

}