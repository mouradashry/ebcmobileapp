import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChangeMpinPage } from './change-mpin';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
	declarations: [
		ChangeMpinPage,
	],
	imports: [
		IonicPageModule.forChild(ChangeMpinPage),
		TranslateModule.forChild()

	],
	exports: [
		ChangeMpinPage
	]
})
export class ChangeMpinPageModule { }
