import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalVariablesService } from '../../app/services/global-variables.service';
import { ApiService } from '../../app/services/api.service';
import { HomePage } from '../home/home';

/**
 * Generated class for the ChangeMpinPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
	selector: 'page-change-mpin',
	templateUrl: 'change-mpin.html',
})
export class ChangeMpinPage {
	old_mPin;
	new_mPin;
	confirm_mPin;
	private changeMpinUrl = '/wallet_users/updateMPin';

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public cdRef: ChangeDetectorRef,
		private global: GlobalVariablesService,
		private api: ApiService) {
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad ChangeMpinPage');
	}


	detectOldMpinChange(value) {
		this.cdRef.detectChanges();
		this.old_mPin = value.length > 6 ? value.substring(0, 6) : value;
	}

	detectNewMpinChange(value) {
		this.cdRef.detectChanges();
		this.new_mPin = value.length > 6 ? value.substring(0, 6) : value;
	}

	detectConfirmMpinChange(value) {
		this.cdRef.detectChanges();
		this.confirm_mPin = value.length > 6 ? value.substring(0, 6) : value;
	}

	back(){
		this.navCtrl.pop();
	}

	async submit() {
		if (!(this.old_mPin && this.new_mPin && this.confirm_mPin)) {
			this.global.presentToast('toast.9', 1);
			return;
		}
		this.global.presentLoading();

		let res = await this.api.postData(this.changeMpinUrl, {
			"mpin": this.old_mPin,
			"newmpin": this.new_mPin,
			"confirm_mpin": this.confirm_mPin
		}).toPromise();

		if (res.status == 200){
			this.global.presentToast(res.data.message);
			this.navCtrl.setRoot(HomePage)
			return;
		}
		return this.global.presentToast(res.data.message);


	}

}
