import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { ApiService } from '../../../app/services/api.service';
import { GlobalVariablesService } from '../../../app/services/global-variables.service';
import { stepOne } from '../step1/step1';
import { addCard } from '../addCard/addCard';

@Component({
  selector: 'page-listCards',
  templateUrl: 'listCards.html',
})
export class listCards implements OnInit {
  cards: any[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private api: ApiService,
    private alertCtrl: AlertController,
    private globals: GlobalVariablesService) {

  }

  async ngOnInit() {

    this.globals.presentLoading();
    let res = await this.api.postData(this.globals.listCardsUrl, {}).toPromise();

    if (res.status !== 200) {

      return this.globals.presentToast(res.data.message);
    }

    this.cards = res.data.cards

  }

  back() {
    this.navCtrl.pop();
  }

  addCard() {
    this.navCtrl.push(addCard);
  }

  async delete(id, index) {
    let alert = this.alertCtrl.create({
      title: 'Confirm delete card',
      message: 'Are you sure you want to delete this card?',
      buttons: [
        {
          text: 'No',
          handler: () => {
          }
        },
        {
          text: 'Yes',
          handler: async () => {
            this.globals.presentLoading();
            let res = await this.api.postData(this.globals.removeCardsUrl, { id: id }).toPromise();
            if (res.status == 200) {
              this.cards.splice(index, 1);
              return this.globals.presentToast(res.data.message);
            }
            return this.globals.presentToast(res.data.message);
          }
        }
      ]
    })
    alert.present();
  }
  async getBalance(card: any) {
    this.globals.presentLoading();
    let data: any = await this.api.postData(this.globals.getBalance, { cardToken: card.cardToken, walletMobileNumber: card.customerMobileNumber }).toPromise();

    let alert = this.alertCtrl.create({
      title: 'your current balance is ',
      message: this.numberWithCommas(data.data.balance),
      buttons: [
        {
          text: 'close',
          handler: () => {
          }
        }
      ]
    })
    alert.present();
  }
  numberWithCommas(number: any) {
    if (number)
      return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return 0;
  }
  credit(card: any) {
    this.navCtrl.push(stepOne, { cardIdentifier: card.cardToken, mobileNumber: card.customerMobileNumber, type: this.globals.CREDIT, maskedCard: card.maskedCard })
  }
  debit(card: any) {
    this.navCtrl.push(stepOne, { cardIdentifier: card.cardToken, mobileNumber: card.customerMobileNumber, type: this.globals.DEBIT, maskedCard: card.maskedCard })
  }

}
