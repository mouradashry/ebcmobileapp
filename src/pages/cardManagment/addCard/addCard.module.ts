import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { addCard } from './addCard';

@NgModule({
  declarations: [
    addCard,
  ],
  imports: [
    IonicPageModule.forChild(addCard),
    TranslateModule.forChild()
  ],
  exports: [
    addCard
  ]
})
export class addCardModule { }
