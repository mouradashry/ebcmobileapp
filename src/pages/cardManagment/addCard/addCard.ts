import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ApiService } from '../../../app/services/api.service';
import { GlobalVariablesService } from '../../../app/services/global-variables.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { SuccessMessage } from '../../success-message/success-message';
import { FailureMessage } from '../../failure-message/failure-message';

@Component({
  selector: 'page-addCard',
  templateUrl: 'addCard.html',
})
export class addCard implements OnInit {

  addCardForm: FormGroup;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private globals: GlobalVariablesService,
    private api: ApiService,
    private formBuilder: FormBuilder) {

  }

  ngOnInit() {
    this.addCardForm = this.formBuilder.group({
      pin: ['', Validators.required],
      pan: ['', Validators.required],
      nationalId: ['', [Validators.maxLength(14), Validators.minLength(14)]],
      walletMobileNumber: ['']
    });
  }
  back() {
    this.navCtrl.pop();
  }

  get nationalId() {
    return this.addCardForm.get('nationalId');
  }

  get pin() {
    return this.addCardForm.get('pin');
  }

  get pan() {
    return this.addCardForm.get('pan');
  }

  async addCard(body) {
    await this.globals.presentLoading();
    let res = await this.api.postData(this.globals.addCardUrl, body).toPromise();
    if (res.status == 200)
      return this.navCtrl.setRoot(SuccessMessage, { message: res.data.message });

    return this.navCtrl.setRoot(FailureMessage, { message: res.data.message });

  }
}
