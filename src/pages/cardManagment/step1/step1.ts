import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ApiService } from '../../../app/services/api.service';
import { GlobalVariablesService } from '../../../app/services/global-variables.service';
import { stepTwo } from '../step2/step2';

@Component({
  selector: 'page-step1',
  templateUrl: 'step1.html',
})
export class stepOne implements OnInit {

  amount: string;
  cardIdentifier: string;
  maskedCard: string;
  mobileNumber: string;
  type: string;
  title: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private cdRef: ChangeDetectorRef,
    public globals: GlobalVariablesService,
    private api: ApiService) { }

  ngOnInit() {

    this.cardIdentifier = this.navParams.get('cardIdentifier')
    this.mobileNumber = this.navParams.get('mobileNumber')
    this.type = this.navParams.get('type');
    this.title = this.type
    this.maskedCard = this.navParams.get('maskedCard')
  }

  back() {

    this.navCtrl.pop()
  }

  async stepOne() {

    let body = { amount: this.amount, type: this.type, maskedCard: this.maskedCard };
    await this.globals.presentLoading();

    let res = this.type === this.globals.CREDIT ? await this.api.postData(this.globals.loadFromCardStep1Url, body).toPromise() : await this.api.postData(this.globals.loadFromWalletStep1Url, body).toPromise();

    if (res.status !== 200)
      return this.globals.presentToast(res.data.error)

    let fees = res.data.senderOk ? res.data.senderOk.totalFees : null;
    let amount = res.data.senderOk ? res.data.senderOk.totalAmount : null;
    let requestRefNum = res.data.requestRefNum;

    return this.navCtrl.push(stepTwo, { cardIdentifier: this.cardIdentifier, fees: fees, amount: amount, requestRefNum: requestRefNum, mobileNumber: this.mobileNumber, type: this.type })
  }

  detectAmountChange(value) {

    this.cdRef.detectChanges();
    this.amount = this.globals.detectChange(value, this.amount, 6);
  }

}
