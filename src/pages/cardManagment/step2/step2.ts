import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ApiService } from '../../../app/services/api.service';
import { GlobalVariablesService } from '../../../app/services/global-variables.service';
import { HomePage } from '../../home/home';
import { FailureMessage } from '../../failure-message/failure-message';
import { SuccessMessage } from '../../success-message/success-message';

@Component({
  selector: 'page-step2',
  templateUrl: 'step2.html',
})
export class stepTwo implements OnInit {

  cardIdentifier: string;
  title: string
  type: string;
  requestRefNum: string;
  amount: string;
  fees: string;
  mpin: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private cdRef: ChangeDetectorRef,
    public globals: GlobalVariablesService,
    private api: ApiService) { }

  ngOnInit() {
    this.cardIdentifier = this.navParams.get('cardIdentifier')
    this.type = this.navParams.get('type');
    this.requestRefNum = this.navParams.get('requestRefNum')
    this.amount = this.navParams.get('amount')
    this.fees = this.navParams.get('fees')
    this.title = this.type;
  }

  back() {
    this.navCtrl.push(HomePage)
  }

  detectMpinChange(value) {
    this.cdRef.detectChanges();
    this.mpin = this.globals.detectChange(value, this.mpin, 6);
  }

  async stepTwo() {

    let body = {
      mpin: this.mpin,
      requestRefNum: this.requestRefNum,
      cardToken: this.cardIdentifier,
      type: this.type
    }

    await this.globals.presentLoading();
    let res = this.type === this.globals.CREDIT ? await this.api.postData(this.globals.loadFromCardStep2Url, body).toPromise() : await this.api.postData(this.globals.loadFromWalletStep2Url, body).toPromise();

    if (res.status != 200)
      return this.navCtrl.push(FailureMessage, { message: res.data.message })

    return this.navCtrl.push(SuccessMessage, { message: res.data.message })

  }

}
