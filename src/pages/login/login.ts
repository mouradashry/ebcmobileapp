import { Component, ChangeDetectorRef, OnInit } from '@angular/core';
import { ViewController, NavController, App, Platform, AlertController, Keyboard } from 'ionic-angular';
import { HomePage } from '../home/home';
import { ForgotMpinPage } from '../forgot-mpin/forgot-mpin';
import { SignupIntermediate } from '../signup-intermediate/signup-intermediate';

import { GlobalVariablesService } from '../../app/services/global-variables.service';
import { ApiService } from '../../app/services/api.service';

import { FCM } from '@ionic-native/fcm';
import { Storage } from '@ionic/storage';
import { TranslationService } from '../../app/services/translation.service';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage implements OnInit {

  ngOnInit() {
    if (this.keyboard.isOpen()) {
      this.alertCtrl.create({
        message: 'keyboard' + this.keyboard.isOpen()
      }).present();
      console.log('keyboar opened');

      console.log(this.keyboard.isOpen());
    }
  }
  device_token: string;
  walletId: string;
  mPin: string;
  subUrl: string = '/wallet_users/login';
  loginSubscribe: any;
  currentPopover: any;
  disableBtn: boolean;
  theme = this.globals.theme;
  src: string = `assets/${this.theme}/logo.png`
  pattern = /^[0-9]*$/


  constructor(public viewCtrl: ViewController, public navCtrl: NavController, private fcm: FCM,
    private cdRef: ChangeDetectorRef, public globals: GlobalVariablesService, private api: ApiService,
    public keyboard: Keyboard, public storage: Storage, private app: App, private platform: Platform,
    public translate: TranslationService, private alertCtrl: AlertController) {
      // this.platform.ready().then((readySource) => {
   
      //   this.fcm.getToken().then(token=>{
      //     console.log(token,"Returned")
      //   });
      // });
   
  }


  back() {
    this.navCtrl.pop();
  }

  //validate login form
  async validate() {
    if (this.walletId) {
      if (!(this.walletId.length == 11 || this.walletId.length == 9))
        this.globals.presentToast("toast.1"/*'Your Wallet Id must be 9 or 11 digits'*/);

      else if (this.mPin) {
        if((/[.,]/).test(this.mPin))
            this.globals.presentToast("toast.26"/*'Your mPIN must not contain be dots or comma'*/);
        else  if (this.mPin.length != 6)
          this.globals.presentToast("toast.2"/*'Your mPIN must be 6 digits'*/);

        else {
          this.disableBtn = true;
          this.globals.presentLoading();

          if (this.platform.is('mobileweb')) {
            return this.login('');
          }

          return  this.getFirebaseToken();
        }

      }
      else if (!this.mPin)
        this.globals.presentToast("toast.3"/*'Please enter mPIN'*/);

    }
    else if (!this.walletId)
      this.globals.presentToast("toast.4"/*'Please enter your Wallet ID'*/);

  }

  //firebase get token
  async getFirebaseToken() {
    try {

      let token = await this.fcm.getToken();
      // this.fcm.onTokenRefresh().subscribe((token)=>{
        if (token != null)
         return this.login(token);
       //  if token is null this means it is not established yet
       else {
         this.globals.dismissLoading();
         this.disableBtn = false;
         this.globals.presentToast('toast.s-w-r');
       }

      // });


    }
    catch (error) {
      this.globals.dismissLoading();
      this.disableBtn = false;
      console.error('Error getting token', error);
      this.globals.presentToast('toast.s-w-r');
    }
  }

  login(device_token) {
    let body = {
      mobile_number: this.walletId,
      mpin: this.mPin,
      device_token: device_token,
      scheme_id: this.globals.appSchemeId
    };

    this.loginSubscribe = this.api.postData(this.subUrl, body, false).subscribe((res) => {
      if (res.status == 200) {
        this.updateStorageData(res, this.walletId);
        // this.setUserLanguage(res.data.lang); // set user language according to his language
      }
      else if (res.status == 400) {
        this.disableBtn = false;
        this.globals.presentToast(res.data.message);
      }
      else {
        this.disableBtn = false;
      }
      this.globals.dismissLoading();
    });

  }

  setUserLanguage(userLang: 'ar' | 'en') {
    userLang = userLang || 'en';
    // this.translate.setLang(userLang);
  }

  //force user to enter valid mPin
  detectPinChange(value) {
    this.cdRef.detectChanges();
    console.log(value);
    if((/[.,]/).test(value)) {
      this.globals.presentToast("toast.26"/*'Your mPIN must not contain be dots or comma'*/);
    } else  if(value.length > 6) {
     this.mPin = value.substring(0, 6)
    } else {
      this.mPin = value;
    }

  }

  //force user to enter valid wallet id
  detectMobileChange(value) {
    this.cdRef.detectChanges();
    this.walletId = value.length > 11 ? value.substring(0, 11) : value;
  }


  async updateStorageData(res, walletId) {

    await this.globals.update('balance', res.data.current_balance);
    await this.globals.update('walletId', walletId);
    await this.globals.update('apiToken', res.data.token);
    await this.globals.update('user_type', res.data.type);
    await this.globals.update('fullname', res.data.fullname);

    this.dismissPopover();
  }

  //remove popover
  dismissPopover() {
    this.viewCtrl.dismiss();
    this.viewCtrl.onDidDismiss(() => {
      this.globals.dismissLoading();
      //navigate to home page
      this.navigateToHome();
    });

    //unsubscribe here because ngOnDestroy doesn't work in popover
    this.loginSubscribe.unsubscribe();
  }


  //go to home page after login
  navigateToHome() {
    //this is how to navigate from a popover
    this.app.getRootNav().setRoot(HomePage);
  }

  forgotMpin() {
    this.navCtrl.push(ForgotMpinPage);
  }

  signUp() {
    this.navCtrl.push(SignupIntermediate);
  }
  changeScheme() {
    let alert = this.alertCtrl.create({
      title: 'Login',
      inputs: [
        {
          name: 'schemeId',
          placeholder: 'Scheme Id',
          value : this.globals.appSchemeId + ""  
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Confirm',
          handler: data => {
            console.log(data.schemeId)
            this.globals.appSchemeId = data.schemeId;
          }
        }
      ]
    });
    alert.present();
  }



}
