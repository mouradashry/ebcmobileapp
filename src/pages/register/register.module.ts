import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Register } from './register';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    Register,
  ],
  imports: [
    IonicPageModule.forChild(Register),
    TranslateModule.forChild()
  ],
  exports: [
    Register
  ]
})
export class RegisterModule {}
