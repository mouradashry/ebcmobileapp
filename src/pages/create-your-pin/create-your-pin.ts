import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { HomePage } from '../home/home';
import { ApiService } from '../../app/services/api.service'
import { FCM } from '@ionic-native/fcm';
import { Storage } from '@ionic/storage';
import { GlobalVariablesService } from '../../app/services/global-variables.service';

@IonicPage()
@Component({
  selector: 'page-create-your-pin',
  templateUrl: 'create-your-pin.html',
})
export class CreateYourPin {

  pin: string = "";
  pin_confirm: string = "";
  dataSub: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public service: ApiService, public fcm: FCM, public storage: Storage,
    public globals: GlobalVariablesService, private cdRef: ChangeDetectorRef, public app: App) {
  }

  async getFirebaseToken() {
    console.log("Get Firebase Token")
    if (this.pin == this.pin_confirm) {
      console.log(this.pin , this.pin_confirm)
      try {
        console.log(this.fcm);
        let token = await this.fcm.getToken();
        console.log(token);
        if (token != null)
          return this.create(token);
        //if token is null this means it is not established yet
        else
          this.globals.presentToast('toast.s-w-r');
      }
      catch (error) {
        console.error('Error getting token', error);
        this.globals.presentToast('toast.s-w-r');
      }
    }
    else{
      console.log("showing toast");
      
      this.globals.presentToast("toast.10");
    }
  }


  create(device_token: any) {
    console.log(device_token)
    if (this.pin.length == 0 || this.pin_confirm.length == 0){
      console.log(this.pin, this.pin_confirm);
      this.globals.presentToast("toast.11", 5);
      
    } else {
      let url = '/wallet_users/createPin';
      this.globals.presentLoading();
      let dataObj = {
        mobile_number: this.navParams.get("walletId"),
        mpin: this.pin,
        device_token: device_token
      }
      console.log(dataObj)
      this.dataSub = this.service.postData(url, dataObj)
        .subscribe(data => {
          console.log(data);
          this.globals.dismissLoading();
          if (data.data.message == "ok")
            this.updateStorageData(data);
          else
            this.globals.presentToast(data.data.message, 5);
        });
    }
  }

  async updateStorageData(data) {
    await this.globals.update('balance', data.data.balance);
    await this.globals.update('walletId', data.data.mobile_number);
    await this.globals.update('apiToken', data.data.token);
    await this.globals.update('user_type', data.data.type);
    this.navCtrl.push(HomePage);
  }

  detectPinChange(value) {
    console.log(value)
    this.cdRef.detectChanges();
    this.pin = this.globals.detectChange(value, this.pin, 6);
  }

  detectPin2Change(value) {
    console.log(value)
    this.cdRef.detectChanges();
    this.pin_confirm = this.globals.detectChange(value, this.pin_confirm, 6);
  }

  ngOnDestroy() {
    if (this.dataSub)
      this.dataSub.unsubscribe();
  }

}
