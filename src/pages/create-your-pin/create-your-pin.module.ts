import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateYourPin } from './create-your-pin';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    CreateYourPin,
  ],
  imports: [
    IonicPageModule.forChild(CreateYourPin),
    TranslateModule.forChild()
  ],
  exports: [
    CreateYourPin
  ]
})
export class CreateYourPinModule {}
