import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import * as qrcode from 'qrcode';
import { GlobalVariablesService } from '../../app/services/global-variables.service';
import { TranslationService } from '../../app/services/translation.service';

/**
 * Generated class for the QrDetailsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-qr-details',
  templateUrl: 'qr-details.html',
})
export class QrDetailsPage {

  @ViewChild('canv') canv;
  qrDetails: any;
  body: any;
  additionalData = {};
  addtionalDataKeys = [];

  additionalDataDisplayName = {
    billNumber: 'Bill id',
    mobileNumber: 'Mobile number',
    storeLabel: 'Store label',
    loyaltyNumber: 'Loyalty number',
    referenceLabel: 'Reference label',
    customerLabel: 'Customer label',
    terminalLabel: 'Terminal label',
    purposeOfTransaction: 'Purpose of transaction'
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public globals: GlobalVariablesService,
    public translate: TranslationService) {
  }

  home() {
    this.navCtrl.push(HomePage);
  }

  back() {
    this.navCtrl.pop();
  }

  async ngOnInit() {
    this.qrDetails = this.navParams.get('qrCode');

    this.body = this.navParams.get('body');

    // setting merchant name in body
    this.body['merchantName'] = await this.globals.get('fullname');

    this.body = Object.assign({}, this.body, { tip: undefined });

    // setting additional data object 
    this.additionalData = this.makeAdditionalDataObject(Object.assign({}, this.body));
    this.addtionalDataKeys = Object.keys(this.additionalData);

    return await this.generateQr(this.qrDetails);
  }

  makeAdditionalDataObject(body: any) {
    delete body['amount'];
    delete body['tip'];
    delete body['qrcodeType'];
    delete body['merchantName'];
    return body;
  }

  async generateQr(payload) {
    await this.generateQrImg(payload);
  }

  generateQrImg(payload) {
    return new Promise(resolve => {
      qrcode.toCanvas(this.canv.nativeElement, payload,
        (err: any) => resolve(err));
    });
  }


  async getAdditionalDataDisplayName(fieldVarName: string) {
    let additionalDataDisplayName = {
      billNumber: 'Bill id',
      mobileNumber: 'Mobile number',
      storeLabel: 'Store label',
      loyaltyNumber: 'Loyalty number',
      referenceLabel: 'Reference label',
      customerLabel: 'Customer label',
      terminalLabel: 'Terminal label',
      purposeOfTransaction: 'Purpose of transaction'
    };

    let result = await this.translate.getValue(additionalDataDisplayName[fieldVarName]);
    return result;
  }


}
