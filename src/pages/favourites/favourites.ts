import { Component, OnInit } from '@angular/core';
import { IonicPage, LoadingController, NavController ,Events} from 'ionic-angular';
import { FavouriteService } from '../../app/services/favourite.service';
import { FavData } from '../../app/services/favourite.service';
import { GlobalVariablesService } from '../../app/services/global-variables.service';
import { MoneyTransfer } from '../../pages/money-transfer/money-transfer';
import { Purchase } from '../../pages/purchase/purchase';
import { CashR2p } from '../../pages/cash-r2p/cash-r2p';

@IonicPage()
@Component({
  selector: 'page-favourites',
  templateUrl: 'favourites.html',
})
export class Favourites implements OnInit {
  favourites:FavData[] = [];
  transactionPages;
  billingPages;
  showNoData = false;
  constructor(private fav: FavouriteService,
    private globals: GlobalVariablesService,
    private navCtrl: NavController,
    private event:Events) {
    this.transactionPages = {
      'p2p': MoneyTransfer,
      'm2m': Purchase,
      'p2m': Purchase,
      'r2p': CashR2p,
      'cashIn': CashR2p,
      'cashOut': CashR2p
    }

    this.billingPages = {};

    this.event.subscribe('fav-name-changed',(new_title,index)=>{
      this.favourites[index].title = new_title;
    });
  }

  back() {
    this.navCtrl.pop();
  }

  async ngOnInit() {
    this.globals.presentLoading();
    let favourites = await this.fav.get();
    if (favourites)
      this.favourites = favourites;

    else if (this.fav.noMoreData && this.fav.offset == 1)
      this.showNoData = true;
     
  }

  async fetchMoreData(infiniteScroll) {
    let newFavourites = await this.fav.get();
    if (newFavourites)
      this.favourites = this.favourites.concat(newFavourites);

    if (this.fav.noMoreData)
      infiniteScroll.enable(false);

    infiniteScroll.complete();
  }

  async delete(id,index){
    await this.fav.delete(id);
    this.favourites.splice(index,1);
  }

  async editName(favourite,index){
    await this.fav.editNameAlert(favourite.id,favourite.title,index);
  }

  navigate(data) {
    this.navCtrl.push(this.getPage(data), { 'favourite': data });
  }

  getPage(data) {
    return this[`${data.service_type}Pages`][data.trx_type];
  }

  ngOnDestroy(){
    this.fav.reset();
  }

}