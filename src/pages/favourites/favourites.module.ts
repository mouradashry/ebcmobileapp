import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Favourites } from './favourites';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
	declarations: [
		Favourites,
	],
	imports: [
		IonicPageModule.forChild(Favourites),
		TranslateModule.forChild()

	],
	exports: [
		Favourites
	]
})
export class FavouritesModule { }
