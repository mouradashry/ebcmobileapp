import { NgModule, ErrorHandler, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpModule, Http } from '@angular/http';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { FCM } from '@ionic-native/fcm';
import { IonicStorageModule } from '@ionic/storage';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Network } from '@ionic-native/network';
import { Contacts } from '@ionic-native/contacts';
import { NFC, Ndef } from '@ionic-native/nfc';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';


// Pages
import { HomePage } from '../pages/home/home';
import { Register } from '../pages/register/register'
import { SignupIntermediate } from '../pages/signup-intermediate/signup-intermediate'
import { Settings } from '../pages/settings/settings'
import { CreateYourPin } from '../pages/create-your-pin/create-your-pin'
import { Verification } from '../pages/verification/verification';
import { Favourites } from '../pages/favourites/favourites';
import { Cash } from '../pages/cash/cash';
import { MoneyTransfer } from '../pages/money-transfer/money-transfer';
import { TransferConfirmation } from '../pages/transfer-confirmation/transfer-confirmation';
import { SuccessMessage } from '../pages/success-message/success-message';
import { FailureMessage } from '../pages/failure-message/failure-message';
import { HistoryDetails } from '../pages/history-details/history-details';
import { Category } from '../pages/category/category';
import { Splash } from '../pages/splash/splash';
import { LogOutPage } from '../pages/logout/logout';
import { TransactionDetails } from '../pages/transaction-details/transaction-details';
import { CashR2p } from '../pages/cash-r2p/cash-r2p';
import { R2p } from '../pages/r2p/r2p';
import { Otp } from '../pages/otp/otp';
import { ServiceProvider } from '../pages/service-provider/service-provider';
import { ServicePage } from '../pages/service/service';
import { VoucherPage } from '../pages/voucher/voucher';
import { BillPage } from '../pages/bill/bill'
import { BilldetailsPage } from '../pages/billdetails/billdetails'
import { NotificationsPage } from '../pages/notifications/notifications';
import { Purchase } from '../pages/purchase/purchase';
import { Nfc } from '../pages/nfc/nfc';
import { LoginPage } from '../pages/login/login';
import { QrDetailsPage } from '../pages/qr-details/qr-details';
import { StatisticsPage } from '../pages/statistics/statistics';
import { OnlineCardsPageModule } from '../pages/online-cards/online-cards.module';
import { VccEnquire } from '../pages/vcc-enquire/vcc-enquire';

//Modules
import { RegisterModule } from '../pages/register/register.module'
import { FavouritesModule } from '../pages/favourites/favourites.module';
import { SignupIntermediateModule } from '../pages/signup-intermediate/signup-intermediate.module'
import { CashModule } from '../pages/cash/cash.module';
import { MoneyTransferModule } from '../pages/money-transfer/money-transfer.module';
import { VerificationModule } from '../pages/verification/verification.module';
import { SettingsModule } from '../pages/settings/settings.module'
import { CreateYourPinModule } from '../pages/create-your-pin/create-your-pin.module'
import { SuccessMessageModule } from '../pages/success-message/success-message.module';
import { FailureMessageModule } from '../pages/failure-message/failure-message.module';
import { TransferConfirmationModule } from '../pages/transfer-confirmation/transfer-confirmation.module';
import { CashR2pModule } from '../pages/cash-r2p/cash-r2p.module';
import { NotificationsPageModule } from '../pages/notifications/notifications.module';
import { ForgotMpinPageModule } from '../pages/forgot-mpin/forgot-mpin.module';
import { ChangeMpinPageModule } from '../pages/change-mpin/change-mpin.module';
import { StaticQrCodeModule } from '../pages/qrcode/static-qrcode/static-qrcode.module';


//Services
import { ApiService } from './services/api.service';
import { GlobalVariablesService } from './services/global-variables.service';
import { DateFormat } from './pipes/dateFormat';
import { FilterHistory } from './pipes/filterHistory';
import { FavouriteService } from './services/favourite.service';
import { NfcService } from './services/nfc.service';
import { ScanQrcodeService } from './services/scanQrcode.service';
import { ContactsService } from './services/contacts.service';
import { TrxStep1Service } from './services/trx-step1.service';
import { TranslationService } from './services/translation.service';
import { DynamicQrCode } from '../pages/qrcode/dynamic-qrcode/dynamic-qrcode';
import { DynamicQrCodeModule } from '../pages/qrcode/dynamic-qrcode/dynamic-qrcode.module';
import { StaticQrCode } from '../pages/qrcode/static-qrcode/static-qrcode';
import { Validator } from './services/validator.service';
import { FeesOnlyConfirmationModule } from '../pages/confirmations/feesOnlyConfirmation/feesOnlyConfirmation.module';
import { FeesOnlyConfirmation } from '../pages/confirmations/feesOnlyConfirmation/feesOnlyConfirmation';
import { VccEnquireComponentModule } from '../pages/vcc-enquire/vcc-enquire.module';
import { listCards } from '../pages/cardManagment/listCards/listCards';
import { addCardModule } from '../pages/cardManagment/addCard/addCard.module';
import { stepOne } from '../pages/cardManagment/step1/step1';
import { stepTwo } from '../pages/cardManagment/step2/step2';


export function createTranslateLoader(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    Splash,
    LogOutPage,
    DateFormat,
    FilterHistory,
    HistoryDetails,
    TransactionDetails,
    R2p,
    Otp,
    ServiceProvider,
    ServicePage,
    VoucherPage,
    BillPage,
    BilldetailsPage,
    Category,
    Purchase,
    Nfc,
    listCards,
    stepOne,
    stepTwo,
    LoginPage,
    QrDetailsPage,
    StatisticsPage,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp, {
      mode: 'ios',
      scrollAssist: true, autoFocusAssist: true
    }),
    addCardModule,
    FavouritesModule,
    CashModule,
    MoneyTransferModule,
    RegisterModule,
    SignupIntermediateModule,
    VerificationModule,
    SettingsModule,
    CreateYourPinModule,
    SuccessMessageModule,
    FailureMessageModule,
    TransferConfirmationModule,
    CashR2pModule,
    NotificationsPageModule,
    OnlineCardsPageModule,
    ForgotMpinPageModule,
    ChangeMpinPageModule,
    DynamicQrCodeModule,
    StaticQrCodeModule,
    FeesOnlyConfirmationModule,
    VccEnquireComponentModule,
    IonicStorageModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [Http]
      }
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    Favourites,
    Cash,
    MoneyTransfer,
    listCards,
    stepOne,
    stepTwo,
    Register,
    SignupIntermediate,
    Verification,
    Settings,
    CreateYourPin,
    Splash,
    SuccessMessage,
    HistoryDetails,
    Category,
    FailureMessage,
    TransferConfirmation,
    LogOutPage,
    TransactionDetails,
    CashR2p,
    R2p,
    Otp,
    ServiceProvider,
    ServicePage,
    VoucherPage,
    BillPage,
    BilldetailsPage,
    Purchase,
    Nfc,
    LoginPage,
    QrDetailsPage,
    StatisticsPage,
    DynamicQrCode,
    StaticQrCode,
    FeesOnlyConfirmation,
    VccEnquire
  ],
  providers: [
    StatusBar,
    SplashScreen,
    FCM,
    ApiService,
    GlobalVariablesService,
    FavouriteService,
    BarcodeScanner,
    Network,
    Contacts,
    NFC,
    Ndef,
    NfcService,
    ScanQrcodeService,
    ContactsService,
    TrxStep1Service,
    TranslationService,
    Validator,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    { provide: LOCALE_ID, useValue: window.localStorage.getItem('lang') || 'en' }
  ]
})
export class AppModule { }


