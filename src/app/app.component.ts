import { Component, ViewChild } from '@angular/core';
import { Platform, Events, AlertController, NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { FCM } from '@ionic-native/fcm';
import { Network } from '@ionic-native/network';

import { Splash } from '../pages/splash/splash';
import { Cash } from '../pages/cash/cash';
import { HistoryDetails } from '../pages/history-details/history-details';
import { LogOutPage } from '../pages/logout/logout';
import { R2p } from '../pages/r2p/r2p';
import { Purchase } from '../pages/purchase/purchase';
import { HomePage } from '../pages/home/home';

import { ApiService } from './services/api.service';
import { GlobalVariablesService } from './services/global-variables.service';
import { NfcService } from './services/nfc.service';
import { TranslationService } from './services/translation.service';

@Component({
	templateUrl: 'app.html',
})
export class MyApp {
	@ViewChild('myNav') nav: NavController;
	rootPage: any;
	splash: any;
	refreshDeviceTokenSubUrl: string = '/wallet_users/updateToken';

	constructor(public platform: Platform, public statusBar: StatusBar, private fcm: FCM,
		private api: ApiService,
		public globals: GlobalVariablesService, public events: Events,
		private alertCtrl: AlertController, private network: Network,
		private nfcService: NfcService,
		public translation: TranslationService,
	) {

		this.initializeApp();

		//logout user when a 401 status returned from the server
		this.events.subscribe('user:logout', () => {
			this.nav.setRoot(LogOutPage);
		});
		//end logout
	}

	initializeApp() {

		this.platform.ready().then(async () => {

			this.translation.setDefault('en');
			//this.translation.toAr();

			this.statusBar.styleDefault();

			this.network.onDisconnect().subscribe(async () => {
				console.log('network was disconnected :-(');
				await this.closeApp();
			});


			await this.checkApiToken();

			//handle notifications
			this.fcm.onNotification().subscribe(async (data) => {
				console.log('notification', data);
				if (this.globals.apiToken)
					await this.handleNotification(data);

			}, error => {
				console.log("notification error: ", error);
			});
			//end handle notifications

			/*
		*event fired when device token is refreshed
		*everytime new token arrive it is sent to backend
		*/
			this.fcm.onTokenRefresh().subscribe(async (new_token: string) => {
				console.log('fcm token from onTokenRefresh', new_token);
				let apiToken = await this.globals.get('apiToken');
				if (new_token && new_token !== '' && apiToken) {
					let body = {
						device_token: new_token
					}
					let apiReq = this.api.postData(this.refreshDeviceTokenSubUrl, body)
						.subscribe(res => apiReq.unsubscribe());
				}

			}, error => {
				console.log('fcm error from onTokenRefresh', error);
			});
			//end refresh token//

			//nfc listening
			await this.listenToNfc();
			//end nfc listening

		});


	}

    /*
	*verify token from backend if token valid log user in(take him to tabs page)
	*if not valid go to login page(take him to splash page)
    */
	async verifyToken(apiToken) {
		let res = await this.api.verifyToken(apiToken).toPromise();
		if (res.status == 200) {
			//log user in
			this.globals.apiToken = apiToken;
			await this.nav.setRoot(HomePage, { hideSplash: true });
		}
		else {
			//token expired log user out			
			await this.gotoLogoutPage();
		}

	}

	presentHistoryConfirmation(title, message) {
		let alert = this.alertCtrl.create({
			title: title,
			message: message,
			buttons: [
				{
					text: 'Dismiss',
					role: 'cancel',
					handler: () => {
						console.log('Cancel clicked');
					}
				},
				{
					text: 'Go to history',
					handler: () => {
						this.nav.push(HistoryDetails);
					}
				}
			],
			enableBackdropDismiss: false
		});
		alert.present();
	}

	async handleNotification(data) {

		let valid = await this.isValidNotification(data.walletId);
		if (!valid) return;
		if (data.cash) {
			await this.api.updateLast4requests();
			await this.nav.setRoot(Cash, { cash: data });
		}
		else if (data.r2p && !this.globals.isDisplayingR2p) {
			this.globals.isDisplayingR2p = true;
			await this.api.updateLast4requests();
			await this.nav.setRoot(R2p, data);
		}

		else if (data.r2p && this.globals.isDisplayingR2p)
			await this.api.updateLast4requests();

		else if (data.history) {
			await this.globals.update('balance', data.balance);

			//Received in background
			if (data.wasTapped)
				await this.nav.push(HistoryDetails);
			//Received in foreground
			else
				this.presentHistoryConfirmation(data.title, data.message);
		}
		else if (data.adjustment) {
			this.globals.presentAlert(data.title, data.message);
			await this.globals.update('balance', data.balance);
		}
		else if (data.alert) {
			this.globals.presentAlert(data.title, data.message);
		}
	}

	/*check if incomming notification is meant to the loggedin user
	*/
	async isValidNotification(walletId) {
		let id = await this.globals.get('walletId');
		if (!(walletId && id)) return false;
		return walletId === id;
	}

	/*
	*get apiToken from DB and verify it
	*every time app starts check if apiToken exist in DB 
	*if exist verify the token else go to login page
	*/
	async checkApiToken() {
		let apiToken = await this.globals.get('apiToken');

		if (apiToken)
			await this.verifyToken(apiToken);
		else {
			await this.gotoLoginPage();
		}
	}

	async gotoLoginPage() {
		await this.nav.setRoot(Splash);
	}

	async gotoLogoutPage() {
		await this.nav.setRoot(LogOutPage);
	}

	async closeApp() {
		await this.gotoLoginPage();
		this.presentNoConnectionAlert();
	}

	async listenToNfc() {
		let nfc: any = await this.nfcService.listenToNfc();
		if (nfc) {
			nfc.subscribe((event) => {
				if (this.globals.apiToken) {
					let message = this.nfcService.getMessage(event);
					if (message)
						return this.nav.push(Purchase, { 'nfc': message });
				}
			});
		}
	}

	presentNoConnectionAlert() {
		let alert = this.alertCtrl.create({
			title: 'No internet connection available',
			buttons: [
				{
					text: 'Close App',
					handler: () => {
						this.platform.exitApp();
					}
				}
			],
			enableBackdropDismiss: false
		});
		alert.present();
	}



}