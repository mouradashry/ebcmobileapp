// import * as glob from 'glob';
// import { promisify } from 'util';
// import { readFileSync } from 'fs';
// let glob_promise = promisify(glob)

// let fs = require('fs');

// module.exports = {
//     theme: 'egbank',

//     getHTML: (filename) => {
//         return fs.readFileSync('/opt/nodejs/heaven/mobile/src/pages/login/login.html', 'utf8');
//         // return filename;
//         // // let result = glob_promise('*.ts');
//         // glob('*.ts', (err, result) => {
//         //     console.log('result', result);
//         // });
//         // return filename;
//     },

//     getCSS: (filename) => {
//         return filename;
//     }
// };

export class Theme {
    static theme = 'egbank';

    static async getHTML(filename: string) {
        // console.log('aaa', require('../../pages/login/login.html!text'));

        // fetch('./service-worker.js').then((data) => data.text()).then((text) => console.log('text', text))

        return `<ion-content no-padding>
        <div class="login-body">
      
      
          <form action="">
            <ion-row class="el-logo">
              <ion-col>
                <img src="assets/the-logo.png">
              </ion-col>
            </ion-row>
      
            <ion-row class="input-row">
              <ion-item class="wallet-id">
                <ion-icon ios="ios-person" md="md-person" item-left></ion-icon>
                <ion-label floating>WALLET ID</ion-label>
                <ion-input type="number" name="walletId" [(ngModel)]="walletId" (ngModelChange)="detectMobileChange($event)"></ion-input>
              </ion-item>
      
              <ion-item class="wallet-pin">
                <ion-icon name="lock" item-left></ion-icon>
                <ion-label floating>
                  <ion-icon name="ion-ios-locked"></ion-icon>mPIN</ion-label>
                <ion-input type="number" style="-webkit-text-security:disc;" name="mPin" [(ngModel)]="mPin" (ngModelChange)="detectPinChange($event)"></ion-input>
              </ion-item>
            </ion-row>
      
      
            <ion-row class="forget-link">
              <ion-col>
                <li (click)="forgotMpin()" class="forget">Forgot mPIN!</li>
              </ion-col>
            </ion-row>
      
            <ion-row class="the-btn">
              <ion-col col-4 offset-2 no-padding>
                <button type="submit" outline block ion-button (click)="validate()" id="valid-btn">Login</button>
              </ion-col>
              <ion-col col-4 no-padding>
                <button type="submit" outline block ion-button (click)="signUp()" id="act-btn">Activate</button>
              </ion-col>
            </ion-row>
      
      
          </form>
      
          <div class="white-space"></div>
      
          <div class="the-bottom">
            <img src="assets/bg2.png">
          </div>
      
      
        </div>
      </ion-content>`;
    }

    static getCSS(filename: string) {
        return filename;
    }
}