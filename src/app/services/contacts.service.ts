import { Injectable} from '@angular/core';
import { GlobalVariablesService} from './global-variables.service';
import { ActionSheetController} from 'ionic-angular';
import { Contacts, Contact } from '@ionic-native/contacts';

@Injectable()
export class ContactsService {

	constructor(
		private contacts: Contacts,
		private globals:GlobalVariablesService,
		public actionSheetCtrl: ActionSheetController){
	}

	async getContact(pageThis,target:string) {
	    try {
	    	let contact: Contact = await this.contacts.pickContact();
	        if (contact.phoneNumbers.length == 0) 
		        this.globals.presentAlert('Error', 'This contact do not have any phone number');
	        
	      	else if (contact.phoneNumbers.length == 1)
	        	pageThis[target] = this.sanitizeNumber(contact.phoneNumbers[0].value);
	        	
	      	else
	      		this.presentActionSheet(contact.phoneNumbers,pageThis,target);
	     
	    }
	    catch (error_code) {
	      console.log('error', error_code);
	      if (error_code == 20)
	        this.globals.presentToast('The application doesn not have permission to open contacts');
	      else if (error_code != 6)
	        this.globals.presentToast('Error while opening conatcts');
	    }
    }

    presentActionSheet(contacts,pageThis,target) {
   		let buttons = [];
   		for(let contact of contacts){
   			contact = this.sanitizeNumber(contact.value)
   			buttons.push({
   				text: contact,
   				handler: () => {
	           		pageThis[target] = contact
	         	}
   			})
   		}
   		buttons.push({
	         text: 'Cancel',
	         role: 'cancel'
	    });
	    let actionSheet = this.actionSheetCtrl.create({
	     title: 'Choose a number',
	     buttons: buttons
	    });

   		actionSheet.present();
 	}

    sanitizeNumber(number){
 		return number.replace(/[^0-9]/g, '').slice(-11);
 	}

}

