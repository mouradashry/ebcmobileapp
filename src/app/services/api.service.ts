import { Injectable } from '@angular/core';
import 'rxjs/Rx';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalVariablesService } from './global-variables.service';
import { Observable } from "rxjs/Observable";
import { Events } from 'ionic-angular';
import { TranslationService } from './translation.service';

@Injectable()
export class ApiService {

  private baseUrl: string;
  private req_timeout: number;

  constructor(private http: Http, private globals: GlobalVariablesService,
    public translate: TranslationService,
    public events: Events) {
    this.baseUrl = globals.Url;
    this.req_timeout = globals.apiReq_timeout * 1000;
  }



  postData(subUrl: string, data: object, withToken = true, handle = true): Observable<any> {
    let headers = new Headers({
      'Content-Type': 'application/json',
      'language': this.translate.lang || this.translate.getCurrentLang()
    });

    if (withToken)
      headers.append('Authorization', `JWT ${this.globals.apiToken}`);


    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(data);

    return this.http.post(this.baseUrl + subUrl, body, options)
      .timeout(this.req_timeout)
      .map(res => {
        this.globals.dismissLoading();
        return { status: res.status, data: res.json() }

      }).catch((err) => {
        return Observable.create((observer) => {
          observer.next(err);
          observer.complete();
        }).map((err) => {

          this.globals.dismissLoading();
          if (!handle) return {};

          if (err.status == 401 || err.status == 403) {
            this[`handle${err.status}Status`]();
            return;
          }

          if (err.status == 0) {
            this[`handle${err.status}Status`]();
            return {};
          }

          if (err.name === "TimeoutError") {
            this.handleTimeout();
            return {};
          }

          else if (err.status < 400 || err.status >= 404)
            this.handleStatus(this.parseJson(err._body));

          return { status: err.status, data: this.parseJson(err._body) }
        });

      });
  }

  getData(subUrl: string, withToken = true, handle = true): Observable<any> {
    let headers = new Headers();
    if (withToken)
      headers.append('Authorization', `JWT ${this.globals.apiToken}`);

    let options = new RequestOptions({ headers: headers });

    return this.http.get(this.baseUrl + subUrl, options)
      .timeout(this.req_timeout)
      .map(res => {
        this.globals.dismissLoading();
        return { status: res.status, data: res.json() }
      }).catch((err) => {

        return Observable.create((observer) => {
          observer.next(err);
          observer.complete();
        }).map((err) => {
          this.globals.dismissLoading();
          if (!handle) return {};
          if (err.status == 401 || err.status == 403) {
            this[`handle${err.status}Status`]();
            return;
          }

          if (err.status == 0) {
            this[`handle${err.status}Status`]();
            return {};
          }

          if (err.name === "TimeoutError") {
            this.handleTimeout();
            return {};
          }

          else if (err.status < 400 || err.status >= 404)
            this.handleStatus(this.parseJson(err._body));

          return { status: err.status, data: this.parseJson(err._body) }
        });

      });
  }

  verifyToken(apiToken): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'application/json' });

    headers.append('Authorization', `JWT ${apiToken}`);

    let options = new RequestOptions({ headers: headers });

    return this.http.post(this.globals.verifyUrl, {}, options)
      .map(res => {
        return { status: res.status, data: res.json() };

      }).catch((err) => {

        return Observable.create((observer) => {
          observer.next(err);
          observer.complete();
        }).map((err) => {

          return { status: err.status, data: this.parseJson(err._body) }
        });

      });
  }

  //get last_4_requests from backend and update them locally
  async updateLast4requests(delRequests?: boolean) {
    let url = '/wallet_users/lastFourRequests';
    let res = await this.getData(url, true, false).toPromise();

    if (res.status !== 200)
      return;

    // getting the last 4 requests
    let last4Requests = await this.globals.get(this.globals.LAST4Requests);

    // intialize if does not exist
    if (!last4Requests)
      last4Requests = {};

    let notifications = {};

    // remaking of the notifications object
    for (let notification of res.data) {
      let refNum = notification.data.refNum;

      // if this is an already existing object add it as it is in the notifications object
      if (last4Requests[refNum])
        notifications[refNum] = Object.assign(notification, last4Requests[refNum]);

      // if this is a new notification added to the object and set viewied to false
      if (!last4Requests[refNum]) {
        notifications[refNum] = notification;
       // notifications[refNum].viewed = false;
      }
    }

    if (res.status == 200) {
      // updating the notifications object
      await this.globals.update(this.globals.LAST4Requests, notifications);
      return notifications;
    }

    if (delRequests)
      return await this.globals.update(this.globals.LAST4Requests, {});
  }

  //Unauthorized no token or token expired
  handle401Status() {
    let message = "toast.19";
    this.globals.presentToast(message);
    return this.events.publish('user:logout');
  }

  //Forbidden has no permission for a specific resource
  handle403Status() {
    let message = "toast.20";
    return this.globals.presentToast(message);
  }

  //handle timeout
  handle0Status() {
    console.log("handle0Status")
    let message = 'toast.s-w-r';
    return this.globals.presentToast(message);
  }

  //500,404 and weird status codes
  handleStatus(data) {
    console.log("handleStatus")
    let message;
    (data.message || data.error)
      ? message = data.message || data.error
      : message = 'toast.s-w-r';
    return this.globals.presentToast(message);
  }

  handleTimeout() {
    let message = 'toast.21';
    return this.globals.presentToast(message);
  }

  parseJson(obj) {
    try {
      obj = JSON.parse(obj);
      return obj;
    }
    catch (err) {
      return {};
    }
  }

}