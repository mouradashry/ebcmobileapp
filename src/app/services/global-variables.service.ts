import { Injectable } from '@angular/core';
import { ToastController, AlertController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslationService } from "./translation.service"


@Injectable()
export class GlobalVariablesService {
  constructor(private toastCtrl: ToastController, private alertCtrl: AlertController,
    private loadingCtrl: LoadingController, public storage: Storage, public translate: TranslationService) { }

  private apiSubUrl: string = '/api/v1/mobile';
  private devUrl = 'https://walletback-staging.finhive.net/backend';
 // private devUrl = 'https://ebcwalletstaging.egyptianbanks.com:8001';
  private localUrl = 'http://localhost:1337';
  // private liveEBCUrl = 'https://ebcwalletstaging.egyptianbanks.com:8001';
  private liveUrl = 'https://walletback-staging.finhive.net/backend';
  public appSchemeId = 2;
  public theme = 'cib';
  public version = '2.4.0';

 // public baseUrl: string = this.devUrl;
 //public baseUrl: string = this.localUrl;
 public baseUrl: string = this.liveUrl;

  public Url: string = this.baseUrl + this.apiSubUrl;
  public apiReq_timeout = 120;//seconds

  /**
   * constants
   */
  LAST4Requests = 'last_4_requests';
  CREDIT = 'Credit';
  DEBIT = 'Debit';
  addCardUrl = '/upm/addCard';
  listCardsUrl = '/upm/listCards';
  removeCardsUrl = '/upm/removeCard';
  loadFromCardStep1Url = '/transaction/loadFromCardStep1';
  loadFromCardStep2Url = '/transaction/loadFromCardStep2';
  loadFromWalletStep1Url = '/transaction/loadFromWalletStep1';
  loadFromWalletStep2Url = '/transaction/loadFromWalletStep2';
  getBalance = '/upm/balanceEnquiry';

  public verifyUrl: string = this.Url + '/verify/token';
  public apiToken: string;
  public user;
  private loader;
  public balance: number;
  public last_4_requests: any = null;
  public walletId: string;
  public fullname: string;
  private user_type: string;
  public isDisplayingR2p: boolean = false;

  async update(name, value) {
    this[name] = value;
    await this.storage.ready();
    this.storage.set(name, value);
  }

  async get(name) {
    if (this[name]) return this[name];
    await this.storage.ready();
    this[name] = await this.storage.get(name);
    return this[name];
  }

  async getTrxType() {
    console.log(await this.get("user_type"))
    if (await this.get("user_type") == "merchant")
      return "m2m";

    return "p2m";
  }

  async delete(name_arr: string[]) {
    for (let name of name_arr) {
      delete this[name];
      await this.storage.remove(name);
    }
  }

  async presentToast(message, duration = 3) {
    message = await this.translate.getValue(message);
    let toast = this.toastCtrl.create({
      message: message,
      duration: duration * 1000,
      position: 'middle'
    });
    toast.present();
  }

  async presentAlert(title, message) {
    let translated = await this.translate.getValue([title, message, 'Dismiss']);
    let alert = this.alertCtrl.create({
      title: translated[title],
      subTitle: translated[message],
      buttons: [translated['Dismiss']]
    });
    alert.present();
  }

  presentLoading() {
    let content = this.translate.getCurrentLang() === 'en' ?
      'Please wait...' :
      'برجاء الانتظار';//arabic loading
    this.loader = this.loadingCtrl.create({
      content: content
    });
    this.loader.present();

  }

  dismissLoading() {
    if (this.loader) {
      this.loader.dismiss();
      this.loader = null;
    }
  }

  detectChange(value, input, length: number) {
    input = value.length > length ? value.substring(0, length) : value;
    return input;
  }

  isValidTip(tip: any) {
    tip = +tip;

    if (tip === 0 || !tip)
      return true;

    if (isNaN(+tip)) {
      this.presentToast("toast.invalidTip");
      return false;
    }

    if (tip <= 0) {
      this.presentToast("toast.negtiveTip");
      return false;
    }

    return true;
  }

  async translateWord(jsonKey: string) {
    return await this.translate.getValue(jsonKey);
  }

  async getAdditionalDataDisplayName(fieldVarName: string) {
    let additionalDataDisplayName = {
      billNumber: 'Bill id',
      mobileNumber: 'Mobile number',
      storeLabel: 'Store label',
      loyaltyNumber: 'Loyalty number',
      referenceLabel: 'Reference label',
      customerLabel: 'Customer label',
      terminalLabel: 'Terminal label',
      pot: 'Purpose of transaction',
      tips: 'Tips'
    };

    return await this.translate.getValue(additionalDataDisplayName[fieldVarName]);
  }

}
