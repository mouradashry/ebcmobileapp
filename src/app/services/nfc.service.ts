import { Injectable} from '@angular/core';
import { GlobalVariablesService} from './global-variables.service';
import { NFC, Ndef } from '@ionic-native/nfc';

@Injectable()
export class NfcService {


	constructor(
		private globals:GlobalVariablesService,
		private nfc: NFC, private ndef: Ndef){
	}

	async listenToNfc() {
	    let nfcAvailable = await this.nfcIsAvailable();
	    if (!nfcAvailable)
	      return nfcAvailable;

	    return this.nfc.addNdefListener(
	      () => {
	        console.log('listening to NFC')
	      },
	      (err) => {
	        console.log('error listening to NFC',err)
	      })
  	}

  	private async nfcIsAvailable() {
	    try {
	      let available = await this.nfc.enabled();
	      console.log('NFC is available', available);
	      return true;
	    }
	    catch (error) {
	      console.log('NFC is not available','error', error);
	      return false;
	    }
  	}

  	getMessage(event){
  		if (!event.tag.ndefMessage){
		    this.globals.presentToast('No data was found');
		    return false;
  		}
        else {
	        let jsonMessage = this.nfc.bytesToString(event.tag.ndefMessage[0].payload);
	        if (jsonMessage) {
	        	jsonMessage = `{${jsonMessage.split("{")[1]}`;
	            let parsedMessage = JSON.parse(jsonMessage);
	            return parsedMessage;
          	}
	        this.globals.presentToast('No data was found');
	        return false;
          
        }
  	}


}