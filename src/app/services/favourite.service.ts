import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { GlobalVariablesService } from './global-variables.service';
import { AlertController, Events } from 'ionic-angular';

@Injectable()
export class FavouriteService {
	private addUrl = '/addToFavourites';
	private getUrl = '/getFavourites';
	private delUrl = '/delFavourites';
	private editNameUrl = '/updateFavourite';
	public offset = 1;
	public noMoreData = false;

	constructor(private api: ApiService,
		private globals: GlobalVariablesService,
		private alertCtrl: AlertController,
		private event: Events) {
	}
	async addandNameFavourite(data: FavData) {
		let alert = this.alertCtrl.create({
			title: await this.globals.translateWord('Favourites'),
			inputs: [
				{
					name: 'Name',
					placeholder: await this.globals.translateWord('Name'),
				}
			],
			buttons: [
				{
					text: await this.globals.translateWord('Cancel'),
					role: 'cancel'
				},
				{
					text: await this.globals.translateWord('Save'),
					handler: name => {
						if (name.Name.length > 15)
							this.globals.presentToast('toast.25');
						else {
							if (/\S/.test(name.Name))  //Matches everything but whitespace
								data['title'] = name.Name;
							this.add(data);
						}
					}
				}
			]
		});
		alert.present();
	}
	async add(data: FavData) {
		if (!this.addIsValid(data))
			return;
		this.globals.presentLoading();
		let res = await this.api.postData(this.addUrl, data).toPromise();
		if (res.status == 200)
			return this.globals.presentToast(res.data.message);
		return this.globals.presentToast(res.data.error);
	}

	async get(): Promise<false | FavData[]> {
		let res = await this.api.postData(this.getUrl, { skip: this.offset }).toPromise();
		if (res.status == 200) {
			if (res.data.favourites.length == 0) {
				this.noMoreData = true;
				return false;
			}
			this.offset = res.data.skip
			return res.data.favourites
		}
		this.globals.presentToast(res.data.error);
		return false;
	}

	async delete(id) {
		this.globals.presentLoading();
		let res = await this.api.postData(this.delUrl, { id: id }).toPromise();
		if (res.status == 200)
			return this.globals.presentToast(res.data.message);
		return this.globals.presentToast(res.data.error);
	}

	async editNameAlert(id, old_name, index) {
		let alert = this.alertCtrl.create({
			title: await this.globals.translateWord('Edit Name'),
			inputs: [
				{
					name: 'Name',
					placeholder: old_name,
				}
			],
			buttons: [
				{
					text: await this.globals.translateWord('Cancel'),
					role: 'cancel'
				},
				{
					text: await this.globals.translateWord('Save'),
					handler: name => {
						if (name.Name.length > 15)
							this.globals.presentToast('toast.25');
						else {
							if (/\S/.test(name.Name))  //Matches everything but whitespace
								this.editName(id, name.Name, index);
							else
								this.globals.presentToast("toast.24");
						}
					}
				}
			]
		});
		alert.present();
	}

	private async editName(id, title, index) {
		this.globals.presentLoading();
		let res = await this.api.postData(this.editNameUrl, { id: id, title: title }).toPromise();
		if (res.status == 200) {
			this.globals.presentToast(res.data.message);
			this.event.publish('fav-name-changed', title, index);
			return;
		}
		return this.globals.presentToast(res.data.error);
	}

	addIsValid(data: FavData) {
		if (!data.receiver && !data.amount) {
			this.globals.presentToast('toast.23')
			return false;
		}
		return true;
	}

	reset() {
		this.offset = 1;
		this.noMoreData = false;
	}


}

export interface FavData {
	title: string,
	trx_type: string,
	receiver: string,
	amount: string,
	service_type: string//'billing'|'transaction',
	data?: any
}