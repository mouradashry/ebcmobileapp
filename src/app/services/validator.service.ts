import { Injectable } from "@angular/core";

@Injectable()
export class Validator {
    constructor() { }


    validations = {
        null: this.isNull,
        length: this.isLength
    };


    applyValidation(field: any, rules: string[]) {

    }

    /**
     * check if a value is empty of null
     * @param text 
     */
    isNull(text: any) {
        // if it was 0 there it does not count as null
        if (text === 0)
            return false;

        // validating that whatever it was if it was null then return true
        if (!text)
            return true;

        // if it was a string and a space was entered then just trim it
        if (typeof text == 'string')
            text = text.trim();

        // then revalidate after trimming to see what happened
        if (!text)
            return true;

        return false;
    }

    /**
     * check length rule
     * @param value 
     * @param length 
     */
    isLength(value: any, length: number) {
        if (value.length === length)
            return true;

        return false;
    }

}