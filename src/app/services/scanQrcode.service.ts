import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { GlobalVariablesService } from './global-variables.service';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { TranslationService } from './translation.service';

@Injectable()
export class ScanQrcodeService {

	private readqrUrl = '/readQrcode';

	constructor(private api: ApiService, private globals: GlobalVariablesService,
		private barcode: BarcodeScanner, private translate: TranslationService) {
	}

	async scan() {
		try {
			let prompt = await this.translate.getValue('alert.qr-service.prompt')
			let options = {
				prompt: prompt
			};

			let qrcodeData: any = await this.barcode.scan(options);
			if (qrcodeData.cancelled)
				return;
			// return this.globals.presentToast('toast.s-w-r');

			await this.globals.presentLoading();
			qrcodeData = qrcodeData.text;
			let res = await this.api.postData(this.readqrUrl, { qrcode: qrcodeData }).toPromise();
			if (res.status == 200) {
				let merchantInfo = res.data.qrData;
				return merchantInfo;
			}

			if (res.status !== 200)
				this.globals.presentToast(res.data.error);
		}
		catch (error) {
			console.log('barcodeData error', error);
			// this.globals.presentToast("toast.22")
		};
		return false;

	}

}
