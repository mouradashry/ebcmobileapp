import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Platform } from 'ionic-angular';

@Injectable()
export class TranslationService {

	public lang: string;

	constructor(
		private translate: TranslateService, private platform: Platform) {
	}

	setDefault(lang: 'ar' | 'en') {
		let default_lang = window.localStorage.getItem('lang') || lang;
		this.translate.setDefaultLang(default_lang);

		if (default_lang === 'ar')
			this.platform.setDir('rtl', true);
		else
			this.platform.setDir('ltr', true);

		this.lang = default_lang;
	}

	toAr() {
		//this.translate.use('ar');
		//this.platform.setDir('rtl', true);
		window.localStorage.setItem('lang', 'ar');
		this.lang = 'ar';
		window.location.reload();
	}

	toEng() {
		//this.translate.use('en');
		//this.platform.setDir('ltr', true);
		window.localStorage.setItem('lang', 'en');
		this.lang = 'en';
		window.location.reload();
	}

	getCurrentLang() {
		return this.translate.defaultLang;
	}

	async getValue(json_key: string | string[], params?: Object) {
		try {
			let value = await this.translate.get(json_key, params).toPromise();
			return value;
		}
		catch (err) {
			return ''
		}

	}

}

