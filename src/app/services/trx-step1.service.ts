import { Injectable} from '@angular/core';
import { GlobalVariablesService} from './global-variables.service';
import { ApiService} from './api.service';

@Injectable()
export class TrxStep1Service {
private url:string='/transaction/checkSender';


	constructor(
		private globals:GlobalVariablesService,
		private api:ApiService){
	}


	async trxStep1(data){

	    this.globals.presentLoading();
	    let transType = await this.getTrxType();
	    data['type'] = transType;
	    let res = await this.api.postData(this.url,data).toPromise();

		if(res.status==200)
			return [
				true,
				{
				  amount:data.amount,
				  fees:res.data.senderOk.totalFees,
				  commissions:res.data.senderOk.totalComms,
				  new_balance:res.data.senderOk.newBalance,
				  totalAmount:res.data.senderOk.totalAmount,
				  walletId:data.recieverMobile,
				  request_ref_num:res.data.requestRefNum,
				  additionalData:res.data.additionalData
				}
			];
			
		else
			return [
				false
				,
				{
					message:res.data.error
				}
			];		    
	   
	}

	public async getTrxType(){
	    let user_type = await this.globals.get('user_type');
	    return user_type === 'customer' ? 'p2m':'m2m';

  	}

}