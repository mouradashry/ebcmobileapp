import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterHistory'
})
export class FilterHistory implements PipeTransform {
  
  transform(requests: any[], walletId: string, senderOrReceiver: string) {
    if (senderOrReceiver && requests) {
      return requests.filter((req) => req[`${senderOrReceiver}_identifier`] == walletId)
    }
    return requests;
  }



}

