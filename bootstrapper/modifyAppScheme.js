let fs = require('fs');
let util = require('util');
const readline = require('readline-sync');

let options = ['1', '2', '3', '12'];
let option = readline.keyInSelect(options, 'Please Choose scheme ?');
let answer = options[option];


let scheme_id = +answer;
let googleServicePath = `google-services.json`;

// getting google service json
// setGoogleServicePackageName(googleServicePath, scheme_id);

// appId in global variable service
let globalVarPath = `./src/app/services/global-variables.service.ts`;
// setGlobalVar(globalVarPath, scheme_id);

// config.xml
let configXmlPath = `config.xml`;
setConfigXml(configXmlPath, scheme_id);


function getSchemeNameAppId(schemeId) {
    schemeId = +schemeId;
    return `com.app.wallet${schemeId}`;
}

/**
 * making changes in google service Json
 */
function setGoogleServicePackageName(path, schemeId) {
    let packageName = getSchemeNameAppId(schemeId);
    let googleService = fs.readFileSync(path, 'utf8');

    let replaceValue = `"package_name": "com.app.wallet${schemeId}"`;

    googleService = googleService.replace(/"package_name": "com.app.wallet(.*)"/g, replaceValue);
    fs.writeFileSync(path, googleService);
}


/**
 * making changes in global variable service
 */
function setGlobalVar(path, schemeId) {
    let globalVar = fs.readFileSync(path, 'utf8');
    let replaceValue = `public appSchemeId = ${schemeId};`;
    globalVar = globalVar.replace(/public appSchemeId = (.*);/g, replaceValue);
    fs.writeFileSync(path, globalVar);
}

/**
 * making changes in config.xml
 */
function setConfigXml(path, schemeId) {
    let config = fs.readFileSync(path, 'utf8');
    let replaceValue = `id="com.app.wallet${schemeId}"`;
    config = config.replace(/id="com.app.wallet(.*)"/g, replaceValue);
    fs.writeFileSync(path, config);
}




let schemeNames = ['Orange Wallet', 'EGbank Wallet', 'CIB Wallet', 'ITS Wallet'];
option = readline.keyInSelect(schemeNames, 'Please app name in view ?');
let schemeName = schemeNames[option];
setConfigXMLSchemeName(schemeName,'config.xml');

function setConfigXMLSchemeName(schemeName,path) {
    let configxml = fs.readFileSync(path, 'utf8');
    configxml = configxml.replace(/\<name\>(.*)\<\/name\>/, `<name>${schemeName}</name>`);
    configxml = configxml.replace(/\<description\>(.*)\<\/description\>/, `<description>${schemeName}</description>`);
    fs.writeFileSync(path, configxml);
}
