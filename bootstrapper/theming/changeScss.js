let fs = require('fs');
let globby = require('glob');
let util = require('util');
let glob = util.promisify(globby);


class changeScss {

    /**
     * IMPORTANT to READ to understand the below code
     * some definition to make you understand the comments:
     * candidates: are files that have a certain  standard they are either default or theme named .scss or .scs files
     * 
     * default files : are just the files that are named like that <component_name>.scss
     * theme named files : are just files that are named like that <component_name>.<theme_name>.scss
     * 
     * 
     * .scs is a created extension for making the scss file close or unseen by the ionic in compilation
     * - there must be only one file open from the candidates files
     * - if no candidate file in the component directory meet the theme name the default file will be taken
     * 
     * 
     * when I say a file is opened it means a file extension is changed from .scs to .scss
     * when file is close it means a file extension is changed from .scss to .scs
     * 
     */

    constructor(themeList, theme) {
        this.themeList = themeList;
        this.theme = theme;
    }

    static async doSomething(themeList, theme) {
        let self = new this(themeList, theme);
        self.applySCSSTheme();
    }

    /**
   * main starting function that will apply the theme
   */
    async applySCSSTheme() {
        let files = await glob('src/pages/**/*.scss'); // get all files inside pages recursive that has .ts extension
        files = await this.getCandidates(files);     // remove all .module.ts files

        for (let file of files) {                    // loop on the file list 
            await this.OperateOnScssFiles(file);
        }
    }

    async OperateOnScssFiles(path) {
        // getting all scs files in component directory
        let scs = await glob(this.getComponentDirectory(path) + `/*.scs`);

        // getting all scss files in component directory that are either default or theme named files
        let scss = await this.getCandidates(await glob(this.getComponentDirectory(path) + `/*.scss`));

        // checking that there is only one scss file that is opened from the candidates which are the default component or theme named scss
        if (scss.length > 1)
            throw Error(`There is a problem in component ${this.getComponentDirectory(path)} please only make 1 scss file from the defaults or theme named files other files could be left as they are`);

        let choosenFile = this.getFileNameWithoutExtension(path) + '.scs';
        if (scs.length)
            choosenFile = this.getThemeFileFromList(scs); // get the candidate file path which should get opened

        let fileToClose = scss[0];

        if (fileToClose.indexOf(`.${this.theme}.`) !== -1 ||
            (choosenFile.indexOf(`${this.getComponentName(choosenFile)}.scs`) !== -1 && fileToClose.indexOf(`${this.getComponentName(fileToClose)}.scss`) !== -1)
        )
            return;

        // change the file that should be working to be .scss
        this.ChangeFileExtension(choosenFile, '.scss');

        // change the file that need to be closed to scs
        this.ChangeFileExtension(fileToClose, '.scs');
    }

    /**
     * apply the open or close operation according to the extension given
     * @param {*} filePath 
     * @param {*} extension 
     */
    ChangeFileExtension(filePath, extension) {
        fs.renameSync(filePath, this.getFileNameWithoutExtension(filePath) + extension);
    }

    /**
     * get the file path without it extension 
     * ex: 
     * input : src/pages/atm.egbank.scss
     * output: src/pages/atm.egbank
     * @param {*} filePath 
     */
    getFileNameWithoutExtension(filePath) {
        let dirPath = this.getComponentDirectory(filePath);
        let splitted = filePath.split('/');
        let fileName = splitted[splitted.length - 1]; // getting last element in the array which is the filename
        splitted = fileName.split('.');
        let fileNameWithoutExtension = splitted.slice(0, splitted.length - 1);
        return `${dirPath}/${fileNameWithoutExtension.join('.')}`;
    }

    /**
     * it receives the component directory scs files and search if any file in them meets the theme name
     * if meets it returns the file path
     * if not if just return the path of the default file 
     * @param {*} fileList 
     */
    getThemeFileFromList(fileList) {
        for (let file of fileList) {
            if (file.indexOf(`.${this.theme}.`) !== -1)
                return file;
        }

        return `${this.getComponentDirectory(fileList[0])}/${this.getComponentName(fileList[0])}.scs`;
    }

    /**
     * getting the candidate files either the default file or theme named files all with extension .scss
     * @param {*} fileList 
     */
    async getCandidates(fileList) {
        return fileList.filter((filePath) => {
            let defaultComponentName = this.getComponentName(filePath);
            return filePath.indexOf(defaultComponentName + '.scss') !== -1 || this.checkIfStringInList(filePath, this.themeList);
        });
    }

    /**
     * check if a string exist in one of the list values 
     * @param {*} text 
     * @param {*} list 
     */
    checkIfStringInList(text, list) {
        for (let item of list) {
            if (text.indexOf(`.${item}.`) !== -1)
                return true;
        }
        return false;
    }

    /**
     * get component name from the scss file path
     * @param {*} path 
     */
    getComponentName(path) {
        let splitMe = path.split('/');
        return splitMe[splitMe.length - 2];
    }

    /**
     * get component directory path from scss file path
     * @param {*} path 
     */
    getComponentDirectory(path) {
        let splitMe = path.split('/');
        return (splitMe.slice(0, splitMe.length - 1)).join('/');
    }
}


module.exports = changeScss;