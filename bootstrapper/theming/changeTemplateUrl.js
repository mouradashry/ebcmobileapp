let fs = require('fs');
let globby = require('glob');
let util = require('util');
let glob = util.promisify(globby);

class changeTemplateUrl {

    /**
     * IMPORTANT NOTES
     * 
     * html candidates are two only
     * either the default html file which is the <component_name>.html 
     * or the theme named html file which is the <component_name>.<theme name>.html
     */

    constructor(theme) {
        this.theme = theme;
    }

    static async doSomething(theme) {
        let self = new this(theme);
        await self.applyTheme();
    }

    /**
     * main starting function that will apply the theme
     */
    async applyTheme() {
        let files = await glob('src/pages/**/*.ts'); // get all files inside pages recursive that has .ts extension
        files = await this.getCandidates(files);     // remove all .module.ts files

        for (let file of files) {                    // loop on the file list 
            this.replaceThemeNameInFile(file);
        }
    }

    /**
     * function that do the main logic of replacing the templateUrls with the new name that will include the theme name
     * @param {*} path 
     */
    async replaceThemeNameInFile(path) {
        let component = fs.readFileSync(path, 'utf8');
        let regex = new RegExp(/templateUrl: \'(.*)\'(,|)/);

        if (component.search(regex) == -1)
            return;

        component = component.replace(regex, `templateUrl: '${(await this.getTemplateUrl(path))}',`);
        fs.writeFileSync(path, component);
    }

    /**
     * function that will get the template url according to the current theme
     * it takes a path get all html files in it and search for an html that match the theme name and return it
     * if no html name was found matched then it will return the default name
     * @param {*} path  
     */
    async  getTemplateUrl(path) {
        let htmls = await this.getHtmlFilesInDirectory(path);      // get all html files in a component directory
        let regex = new RegExp(`\.${this.theme}\.`);               // make a dynamic regex according to theme
        let htmlPath = htmls.find(item => regex.test(item));  // test regex on the html files path list

        if (htmlPath)     // if html found return the new templateUrl 
            return this.getTemplateUrlName(htmlPath);

        return this.getComponentName(path); // not found return default template which is <component name>.html
    }

    /**
     * function that will remove the component name from path and get all html files in the component's directory
     * @param {*} path 
     */
    async  getHtmlFilesInDirectory(path) {
        let orginalPath = path;
        path = path.split('/');
        let pathLength = (path[path.length - 1]).length;
        orginalPath = orginalPath.slice(0, orginalPath.length - pathLength);
        return await glob(`${orginalPath}/*.html`);
    }

    /**
     * take a component path and return the component name with html extension
     * @param {*} path 
     */
    getComponentName(path) {
        path = path.split('/')
        path = path[path.length - 1];
        let splitter = path.split('.');
        return splitter[0] + '.html';
    }

    /**
     * get template url name from path cause it will look like that component.theme.html 
     * so it needs a different treatment that what the above function is doing 
     * @param {*} path 
     */
    getTemplateUrlName(path) {
        path = path.split('/');
        return path[path.length - 1];
    }

    /**
     * remove all paths that include .module. because we dont need them
     * @param {*} fileList 
     */
    async getCandidates(fileList) {
        return fileList.filter((filePath) => {
            return !/\.module\./.test(filePath);
        });
    }

}

module.exports = changeTemplateUrl;