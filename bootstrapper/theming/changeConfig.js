let fs = require('fs');
const readline = require('readline-sync');

class changeConfig {


    static async doSomething(theme, path) {
        let configXML = fs.readFileSync(path, 'utf8');

        // replace android icons
        configXML = configXML.replace(/android\/(.*)\/icon/g, `android/${theme}/icon`);

        // replace android splash
        configXML = configXML.replace(/android\/(.*)\/splash/g, `android/${theme}/splash`);

        // replace ios icons
        configXML = configXML.replace(/ios\/(.*)\/icon/g, `ios/${theme}/icon`);

        // replace ios splash
        configXML = configXML.replace(/ios\/(.*)\/splash/g, `ios/${theme}/splash`);

        fs.writeFileSync(path, configXML);
    }
}


module.exports = changeConfig;