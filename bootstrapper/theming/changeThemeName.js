let fs = require('fs');
let globby = require('glob');
let util = require('util');
let glob = util.promisify(globby);

class changeThemeName { 

    
    constructor(theme) {
        this.theme = theme;
    }

     static doSomething(theme) {
        let globalVarPath = `./src/app/services/global-variables.service.ts`;
        let globalVar = fs.readFileSync(globalVarPath, 'utf8');
        let replaceValue = `public theme = '${theme}';`;
        globalVar = globalVar.replace(/public theme = (.*);/g, replaceValue);
        fs.writeFileSync(globalVarPath, globalVar);
    }
}

module.exports = changeThemeName;