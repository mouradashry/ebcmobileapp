let fs = require('fs');
let globby = require('glob');
let util = require('util');
let glob = util.promisify(globby);

class changeSassVars {


    constructor(theme) {
        this.theme = theme;
    }

    static doSomething(theme) {
        let globalVarPath = `./src/theme/theme.scss`;
        let globalVar = fs.readFileSync(globalVarPath, 'utf8');
        let replaceValue = `$theme: '${theme}';`;
        globalVar = globalVar.replace(/\$theme: (.*);/g, replaceValue);
        fs.writeFileSync(globalVarPath, globalVar);
    }
}

module.exports = changeSassVars;