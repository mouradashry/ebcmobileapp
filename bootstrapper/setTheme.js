let fs = require('fs');
const readline = require('readline-sync');
let globby = require('glob');
let util = require('util');
let glob = util.promisify(globby);
let changeConfig = require('./theming/changeConfig');
let changeTemplateUrl = require('./theming/changeTemplateUrl');
let changeScss = require('./theming/changeScss');
let changeThemeName = require('./theming/changeThemeName');
let changeSassVars = require('./theming/changeSassVars');

let path = 'config.xml';
let options = [
    'egbank',
    'cib',
    'orange',
    'wee',
    'its'
];

let option = readline.keyInSelect(options, 'Choose the app theme ?');
let theme = options[option];


changeConfig.doSomething(theme, path)
    .then(() => changeTemplateUrl.doSomething(theme))
    .then(() => changeScss.doSomething(options, theme))
    .then(() => changeThemeName.doSomething(theme))
    .then(() => changeSassVars.doSomething(theme))
    .then(() => console.log('finished'))
    .catch(err => console.log(err));




