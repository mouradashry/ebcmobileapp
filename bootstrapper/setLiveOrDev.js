let fs = require('fs');
const readline = require('readline-sync');

let path = './src/app/services/global-variables.service.ts';
let options = [
    'dev',
    'live',
    'custom'
];

let option = readline.keyInSelect(options, 'Choose an option to set mobile url ?');
let choosen = options[option];

if (choosen == 'dev')
    replaceDev();

if (choosen == 'live')
    replaceLive();

if (choosen == 'custom') {
    let url = readline.question('Enter url: ');
    makeCustomUrl(url);
}

function replaceDev() {
    commentLive();
    unCommentDev();
}

function replaceLive() {
    commentDev();
    unCommentLive();
}

function makeCustomUrl(url) {
    replaceLive();
    let globalVariableService = fs.readFileSync(path, 'utf8');
    let newGlobalVarService = globalVariableService.replace(/private liveUrl = '(.*)';/, `private liveUrl = '${url}';`);
    fs.writeFileSync(path, newGlobalVarService);
}

function commentLive() {
    let globalVariableService = fs.readFileSync(path, 'utf8');

    if (globalVariableService.search(/\/\/ public baseUrl: string = this\.liveUrl;/) == -1) {
        newGlobalVarService = globalVariableService.replace(/public baseUrl: string = this\.liveUrl;/, '// public baseUrl: string = this.liveUrl;');
        fs.writeFileSync(path, newGlobalVarService);
    }
}

function commentDev() {
    let globalVariableService = fs.readFileSync(path, 'utf8');

    if (globalVariableService.search(/\/\/ public baseUrl: string = this\.devUrl;/) == -1) {
        newGlobalVarService = globalVariableService.replace(/public baseUrl: string = this\.devUrl;/, '// public baseUrl: string = this.devUrl;');
        fs.writeFileSync(path, newGlobalVarService);
    }
}

function unCommentLive() {
    let globalVariableService = fs.readFileSync(path, 'utf8');

    if (globalVariableService.search(/\/\/ public baseUrl: string = this\.liveUrl;/) != -1) {
        newGlobalVarService = globalVariableService.replace(/\/\/ public baseUrl: string = this\.liveUrl;/, 'public baseUrl: string = this.liveUrl;');
        fs.writeFileSync(path, newGlobalVarService);
    }
}

function unCommentDev() {
    let globalVariableService = fs.readFileSync(path, 'utf8');

    if (globalVariableService.search(/\/\/ public baseUrl: string = this\.devUrl;/) != -1) {
        newGlobalVarService = globalVariableService.replace(/\/\/ public baseUrl: string = this\.devUrl;/, 'public baseUrl: string = this.devUrl;');
        fs.writeFileSync(path, newGlobalVarService);
    }
}



